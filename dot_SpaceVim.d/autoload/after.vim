function! after#after() abort 
  " Use K to show documentation in preview window.
  nnoremap <silent> gh :call <SID>show_documentation()<CR>
  " Use Ctrl Space to trigger completions
  if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
  else
    inoremap <silent><expr> <c-@> coc#refresh()
  endif

  " Highlight the symbol and its references when holding the cursor.
  autocmd CursorHold * silent call CocActionAsync('highlight')
 
  " Symbol renaming.
  nmap <leader>rn <Plug>(coc-rename)

  function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
      execute 'h '.expand('<cword>')
    else
      call CocAction('doHover')
    endif
  endfunction 
endfunction
