map <Space> <Leader>

" noremap <unique> ' <NOP>
" let mapleader = "\'"
" let maplocalleader = "\'"

" use semicolon instead of colon for commands. one less keystroke.
nnoremap ; :

"""""""""""""""""""
" File Management
"""""""""""""""""""
" Save.
map <C-s> :w<CR>
imap <C-s> <ESC>:w<CR>
" Quit
nmap <leader>q :q<CR>
" nmap <leader>. :qa<CR>
" Line Navigation
"  {num}+ {num}-
"  find way to map the + command to =, because I don't want to hit shift

" Copy / Paste out / into vim
noremap <leader>y "+y
noremap <leader>p "+p

imap <C-BS> <C-W>
cmap <C-BS> <C-W>
tmap <C-BS> <C-W>

" Search
"
" https://github.com/dyng/ctrlsf.vim#key-maps
" Enter - Open
" <C-O> - Open, but horizontal split
" O - Open, but leaze the search results
" q - quit
" nmap <C-S-f> <Plug>CtrlSFPrompt
" vmap <C-S-f> <Plug>CtrlSFVwordExec
"
"""

nnoremap <silent> <CR> :noh<CR><CR>

"""""""""""""""""
" Buffers
"""""""""""""""""
nnoremap <silent> <C-Tab> :bnext<CR>
nnoremap <silent> <C-S-Tab> :bprevious<CR>
" inoremap <silent> ]] <Esc>:bnext<CR>
" inoremap <silent> [[ <Esc>:bprevious<CR>
" nnoremap <silent> ]] :bnext<CR>
" nnoremap <silent> [[ :bprevious<CR>
nnoremap <silent> <C-h> :bprevious<CR>
nnoremap <silent> <C-l> :bnext<CR>
inoremap <silent> <C-h> <Esc>:bprevious<CR>
inoremap <silent> <C-l> <Esc>:bnext<CR>

nnoremap <silent> qq :Bdelete<CR>
nnoremap <silent> qQ :%bdelete<CR>

inoremap jk <Esc>

""""""""""""""""""
" Pane Management
""""""""""""""""""
" Convert Pane to Terminal
nnoremap <C-y> :terminal<CR>

" " Creating Panes
" " chtn = up, left, down, right
" nnoremap <leader>c :leftabove new<CR>
" nnoremap <leader>h :leftabove vnew<CR>
" nnoremap <leader>t :rightbelow new<CR>
" nnoremap <leader>n :rightbelow vnew<CR>

" " Switching Panes
" nnoremap <A-c> <C-w><C-k>
" nnoremap <A-h> <C-w><C-h>
" nnoremap <A-t> <C-w><C-j>
" nnoremap <A-n> <C-w><C-l>

" Resizing Panes
" increase / decrease width or the "vertical split"
" nnoremap <S-h> :vertical resize -10<CR>
" nnoremap <S-n> :vertical resize +10<CR>

" increase / decrease height or the "horizontal split"
nnoremap <S-t> :resize -10<CR>
nnoremap <S-c> :resize +10<CR>


" Swap: Ctrl+W R
" Resizing
" Ctrl+w _ max out height of current split
" Ctrl+w | max out width of current split
" Ctrl+w = normalize all splits
" Ctrl+W o close every window but current

""""""""""""""""""""
" Spell Checking
""""""""""""""""""""
" nnoremap <F2> :setlocal spell! spelllang=en_us<CR>

"""""""""""""""""""""
" Working with Code
"""""""""""""""""""""
nnoremap "+p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
nnoremap "*p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p
xnoremap "+y y:call system("wl-copy", @")<cr>

" Recursively unfold
nnoremap <leader>z zczA

" Move Lines Up/Down
" nnoremap <C-j> :move+1<CR>
" nnoremap <C-k> :move-2<CR>

" Insert blank line above and below cursor
nmap <silent> ]<Space> o<CR><Up><Esc>:lua require("smart-cursor").indent_cursor()<cr>a
" Indent cursor
nmap <silent> <leader><Tab> :lua require("smart-cursor").indent_cursor()<cr>a

" Close help windows with "q"
autocmd FileType help nnoremap <silent> <buffer> q :q<CR>
