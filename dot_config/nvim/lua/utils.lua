local M = {}

function M.get_typescript_server_path(root_dir)
	local lspconfig_util = require("lspconfig.util")
	local global_ts = "/home/tamim/.volta/tools/shared/typescript/lib/tsserverlibrary.js"
	-- Alternative location if installed as root:
	-- local global_ts = '/usr/local/lib/node_modules/typescript/lib/tsserverlibrary.js'
	local found_ts = ""
	local function check_dir(path)
		found_ts = lspconfig_util.path.join(path, "node_modules", "typescript", "lib", "tsserverlibrary.js")
		if lspconfig_util.path.exists(found_ts) then
			return path
		end
	end

	if lspconfig_util.search_ancestors(root_dir, check_dir) then
		return found_ts
	else
		return global_ts
	end
end

function M.shuffle_table(t)
	local tbl = {}
	for i = 1, #t do
		tbl[i] = t[i]
	end
	for i = #tbl, 2, -1 do
		local j = math.random(i)
		tbl[i], tbl[j] = tbl[j], tbl[i]
	end
	return tbl
end

function M.coc_diagnostic_count_str()
	local diagnostics = vim.api.nvim_buf_get_var(0, "coc_diagnostic_info") -- equivalent to b:coc_diagnostic_info
	if diagnostics ~= nil then
		local total = diagnostics.information + diagnostics.hint + diagnostics.warning + diagnostics.error
		if total >= 9 then
			return "9+"
		else
			return tostring(total)
		end
	end
end

function M.get_sign(name)
	return vim.fn.sign_getdefined(name)[1]["text"]
end

function M.get_hl_color(group)
	return vim.api.nvim_get_hl_by_id(vim.fn.hlID(group), true)
end

function M.unescape_entities(str)
	return str
		:gsub("&quot;", '"')
		:gsub("&nbsp;", " ")
		:gsub("&iexcl;", "¡")
		:gsub("&cent;", "¢")
		:gsub("&pound;", "£")
		:gsub("&curren;", "¤")
		:gsub("&yen;", "¥")
		:gsub("&brvbar;", "¦")
		:gsub("&sect;", "§")
		:gsub("&uml;", "¨")
		:gsub("&copy;", "©")
		:gsub("&ordf;", "ª")
		:gsub("&laquo;", "«")
		:gsub("&not;", "¬")
		:gsub("&shy;", "­")
		:gsub("&reg;", "®")
		:gsub("&macr;", "¯")
		:gsub("&deg;", "°")
		:gsub("&plusmn;", "±")
		:gsub("&sup2;", "²")
		:gsub("&sup3;", "³")
		:gsub("&acute;", "´")
		:gsub("&micro;", "µ")
		:gsub("&para;", "¶")
		:gsub("&middot;", "·")
		:gsub("&cedil;", "¸")
		:gsub("&sup1;", "¹")
		:gsub("&ordm;", "º")
		:gsub("&raquo;", "»")
		:gsub("&frac14;", "¼")
		:gsub("&frac12;", "½")
		:gsub("&frac34;", "¾")
		:gsub("&iquest;", "¿")
		:gsub("&Agrave;", "À")
		:gsub("&Aacute;", "Á")
		:gsub("&Acirc;", "Â")
		:gsub("&Atilde;", "Ã")
		:gsub("&Auml;", "Ä")
		:gsub("&Aring;", "Å")
		:gsub("&AElig;", "Æ")
		:gsub("&Ccedil;", "Ç")
		:gsub("&Egrave;", "È")
		:gsub("&Eacute;", "É")
		:gsub("&Ecirc;", "Ê")
		:gsub("&Euml;", "Ë")
		:gsub("&Igrave;", "Ì")
		:gsub("&Iacute;", "Í")
		:gsub("&Icirc;", "Î")
		:gsub("&Iuml;", "Ï")
		:gsub("&ETH;", "Ð")
		:gsub("&Ntilde;", "Ñ")
		:gsub("&Ograve;", "Ò")
		:gsub("&Oacute;", "Ó")
		:gsub("&Ocirc;", "Ô")
		:gsub("&Otilde;", "Õ")
		:gsub("&Ouml;", "Ö")
		:gsub("&times;", "×")
		:gsub("&Oslash;", "Ø")
		:gsub("&Ugrave;", "Ù")
		:gsub("&Uacute;", "Ú")
		:gsub("&Ucirc;", "Û")
		:gsub("&Uuml;", "Ü")
		:gsub("&Yacute;", "Ý")
		:gsub("&THORN;", "Þ")
		:gsub("&szlig;", "ß")
		:gsub("&agrave;", "à")
		:gsub("&aacute;", "á")
		:gsub("&acirc;", "â")
		:gsub("&atilde;", "ã")
		:gsub("&auml;", "ä")
		:gsub("&aring;", "å")
		:gsub("&aelig;", "æ")
		:gsub("&ccedil;", "ç")
		:gsub("&egrave;", "è")
		:gsub("&eacute;", "é")
		:gsub("&ecirc;", "ê")
		:gsub("&euml;", "ë")
		:gsub("&igrave;", "ì")
		:gsub("&iacute;", "í")
		:gsub("&icirc;", "î")
		:gsub("&iuml;", "ï")
		:gsub("&eth;", "ð")
		:gsub("&ntilde;", "ñ")
		:gsub("&ograve;", "ò")
		:gsub("&oacute;", "ó")
		:gsub("&ocirc;", "ô")
		:gsub("&otilde;", "õ")
		:gsub("&ouml;", "ö")
		:gsub("&divide;", "÷")
		:gsub("&oslash;", "ø")
		:gsub("&ugrave;", "ù")
		:gsub("&uacute;", "ú")
		:gsub("&ucirc;", "û")
		:gsub("&uuml;", "ü")
		:gsub("&yacute;", "ý")
		:gsub("&thorn;", "þ")
		:gsub("&yuml;", "ÿ")
		:gsub("&euro;", "€")
		:gsub(str, "&#(%d+);", function(n)
			return string.char(n)
		end)
		:gsub(str, "&#x(%d+);", function(n)
			return string.char(tonumber(n, 16))
		end)
		:gsub(str, "&amp;", "&") -- Be sure to do this after all others
end

function M.get_ts_context()
	local status = vim.fn["nvim_treesitter#statusline"](60)
	if status == vim.NIL then
		return nil
	end
	return status
end

function M.fetch_json(url)
	local curl = require("plenary.curl")
	vim.pretty_print(curl)
	local request = curl.get(url)
	vim.pretty_print(request)
	if not request.status == 200 then
		error("Error fetching in function utils.fetch_json()")
	end

	return vim.json.decode(url)
end

function M.set_highlights(tbl)
	for _, v in ipairs(tbl) do
		vim.api.nvim_set_hl(0, v[1], v[2])
	end
end

function M.disable_builtin_plugins(plugins)
	for _, plugin in pairs(plugins) do
		vim.g["loaded_" .. plugin] = 1
	end
end

function M.disable_builtin_providers(providers)
	for _, provider in ipairs(providers) do
		vim.g["loaded_" .. provider .. "_provider"] = 1
	end
end

local function init_mapping(mode)
	return function(lhs, rhs, opts)
		vim.keymap.set(mode, lhs, rhs, opts)
	end
end

function M.init_mapping_with_prefix(prefix, mode)
	return function(lhs, rhs, opts)
		vim.keymap.set(mode, prefix .. lhs, rhs, opts)
	end
end

M.map = init_mapping({ "n", "v", "o" })
M.nmap = init_mapping("n")
M.imap = init_mapping("i")
M.xmap = init_mapping("x")

return M
