local M = {}

local cmd = vim.cmd
local o = vim.o -- set
local bo = vim.bo -- buffer-local options, eq setlocal
local wo = vim.wo -- window-local options
local raw = vim.api.nvim_command

function M.setup()
  if vim.fn.exists("+termguicolors") == 1 then
    -- TODO check if this works
    --[[ vim["&t_8f"] = "<Esc>[38;2;%lu;%lu;%lum"
    vim[" &t_8b"] = "<Esc>[48;2;%lu;%lu;%lum" ]]
    o.termguicolors = true
  end

-----
-- UI
-----
  cmd "colorscheme sonokai"
  vim.g["loaded_netrw"] = 0

  if vim.fn.has("nvim") == 1 then
    vim.fn.setenv("NVIM_TUI_ENABLE_TRUE_COLOR", 1)
  end
---------
-- EDITOR
---------
  cmd "syntax enable"
  o.re = 0 -- use new highlighting engine
  o.encoding = "utf-8"
  o.guifont = "JetBrains Mono:h12"
  o.cmdheight = 1
  o.mouse = "a" -- use mouse for everything
  o.showmode = false  -- do not show current mode
  o.laststatus = 2  -- always display statusline
  o.cursorline = true -- highlight current line
  o.ruler = true
  o.number = true  -- show line numbers
  raw([[
    autocmd InsertEnter * :set norelativenumber
    autocmd InsertLeave * :set relativenumber
  ]]) -- relative numbers only in normal mode

  o.backspace = "indent,eol,start"  -- backspace everywhere
  o.autoindent = true -- auto indent for newline
  bo.tabstop = 2
  bo.shiftwidth = 2
  bo.expandtab = true
  wo.linebreak = true -- break lines on words instead of in middle
  wo.breakindent = true -- wrapping lines is indented

  o.hlsearch = true  -- highlight search terms
  wo.list = true  -- show whitespace

  o.autoread = true -- auto reload files on change

  wo.scrolloff = 5 -- minmum lines above and below cursor

  raw([[
    silent !mkdir ~/.local/share/nvim/_backup/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/_temp/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/_undo/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/spell/ > /dev/null 2>&1
  ]]) -- backup and temp

  o.backupdir = "$HOME/.local/share/nvim/_backup" -- backup directory
  o.directory = "$HOME/.local/share/nvim/_temp" -- swap directory

  -- persist undo history
  if vim.fn.has("persistent_undo") == 1 then
    o.undodir = "~/.local/share/nvim/_undo"
    o.undofile = true
  end

  -- goto last editing position
  vim.api.nvim_command([[

  ]])

  -- highlight on yank
  --[[ vim.api.nvim_command([[
augroup highlight_yank
  autocmd!
  au TextYankPost * silent! lua vim.highlight.on_yank { higroup="DiffChange", timeout=200 }
augroup END
  ]]
  --]]

----------------
-- SPELL CHECKER
----------------
  o.spellfile = "~/.local/share/nvim/spell/en_us.utf-8.add"

----------------
-- PANES/BUFFERS
----------------
  o.splitright = true
  o.equalalways = false -- prevents splits from all auto-adjusting horizontally when one closes

  o.foldmethod = "indent" -- fold based on indentation
  o.foldlevel = 99
  o.foldenable = false  -- don't open a file with folds, display the whole thing
  o.signcolumn = "auto" -- always show the signcolumn
  o.wrap = false

  -- set the title of the window to the filename
  o.title = true
  o.titlestring = "%f%( [%M]%)"
end

return M
