local M = {}

function M.setup()
	local packer = require("packer")

	packer.startup(function(use)
		-- Packer managing itself
		use("wbthomason/packer.nvim")

		---------------
		-- Syntax/theme
		---------------
		use({
			"sainnhe/sonokai",
			config = function()
				vim.g["sonokai_style"] = "atlantis"
				vim.g["sonokai_enable_italic"] = 1
				vim.g["sonokai_disable_italic_comment"] = 1
			end,
		})

		-- pug syntax
		use("digitaltoad/vim-pug")

		---------
		-- Editor
		---------
    -- highlight color literals like red, #cba22f
    use({
      "DarwinSenior/nvim-colorizer.lua",
      config = function ()
        require("colorizer").setup({
          "*",
          css = { rgb_fn = true },
        })
      end
    })

		-- highlight rogue whitespaces
		use({
			"ntpeters/vim-better-whitespace",
			config = function()
				-- remove whitespaces when saving
				vim.cmd("autocmd BufWritePre * StripWhitespace")
			end,
		})
		-- start page
    use({
      "goolord/alpha-nvim",
      requires = { "kyazdani42/nvim-web-devicons" },
      config = function ()
        require("alpha").setup(require("alpha.themes.startify").opts)
      end
    })

		------------------------------------
		-- File/project finding & navigation
		------------------------------------

		-- Multi purpose filter
		use({
			"nvim-telescope/telescope.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				local telescope = require("telescope")
				local actions = require("telescope.actions")
				telescope.setup({
					defaults = {
						mappings = {
							i = {
								["<esc>"] = actions.close,
							},
						},
					},
					pickers = {
						lsp_references = {
							theme = "ivy",
						},
					},
				})
				telescope.load_extension("coc")

				-- bindings
				vim.api.nvim_set_keymap(
					"n",
					"<leader>ff",
					"<cmd>lua require('telescope.builtin').find_files()<cr>",
					{ silent = true }
				)
				vim.api.nvim_set_keymap(
					"n",
					"<C-p>",
					"<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_ivy())<cr>",
					{ silent = true }
				)
				vim.api.nvim_set_keymap(
					"n",
					"<leader>fg",
					"<cmd>lua require('telescope.builtin').live_grep(require('telescope.themes').get_ivy())<cr>",
					{ silent = true }
				)
				vim.api.nvim_set_keymap(
					"n",
					"<leader>fb",
					"<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown())<cr>",
					{ silent = true }
				)
				vim.api.nvim_set_keymap(
					"n",
					"<leader>fh",
					"<cmd>lua require('telescope.builtin').help_tags()<cr>",
					{ silent = true }
				)
			end,
		})

		-- project-wide search and replace
		--[[ use({
			"windwp/nvim-spectre",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				vim.api.nvim_set_keymap("n", "<C-S-H>", ":lua require('spectre').open()<CR>", { noremap = true })
				vim.api.nvim_set_keymap("n", "<leader>rr", ":lua require('spectre').open()<CR>", { noremap = true })
				vim.api.nvim_set_keymap(
					"v",
					"<leader>rv",
					":lua require('spectre').open_visual()<CR>",
					{ noremap = true }
				)
				vim.api.nvim_set_keymap(
					"n",
					"<leader>rp",
					"viw:lua require('spectre').open_file_search()<CR>",
					{ noremap = true }
				)
			end,
		}) ]]

		-- surround word
		use({
			"blackCauldron7/surround.nvim",
			config = function()
				require("surround").setup({
					mappings_style = "surround",
				})
			end,
		})

    -- tabout from surrounds
    use({
      "abecodes/tabout.nvim",
      config = function ()
        require("tabout").setup({})
      end,
      wants = {'nvim-treesitter'}
    })

    -- autopair
		use({
			"windwp/nvim-autopairs",
			config = function()
				require("nvim-autopairs").setup({})
			end,
		})

		-- commenting
		use({
			"b3nj5m1n/kommentary",
			config = function()
				require("kommentary.config").configure_language("typescriptreact", {
					single_line_comment_string = "auto",
					multi_line_comment_strings = "auto",
					hook_function = function()
						require("ts_context_commentstring.internal").update_commentstring()
					end,
				})
				require("kommentary.config").configure_language("html", {
					single_line_comment_string = "auto",
					multi_line_comment_strings = "auto",
					hook_function = function()
						require("ts_context_commentstring.internal").update_commentstring()
					end,
				})
				vim.api.nvim_set_keymap("n", "gcc", "<Plug>kommentary_line_default", {})
				vim.api.nvim_set_keymap("i", "<C-_>", "<Plug>kommentary_line_default", { noremap = true })
				vim.api.nvim_set_keymap("n", "<C-/>", "<Plug>kommentary_line_default", {})
				vim.api.nvim_set_keymap("i", "<C-/>", "<Plug>kommentary_line_default", {})
			end,
		})

		-- sneak plugin
		use({
			"ggandor/lightspeed.nvim",
		})

		-- popup keybindings
		use({
			"folke/which-key.nvim",
			config = function()
				local wk = require("which-key")
				wk.setup({
					plugins = {
						spelling = {
							enabled = true,
						},
					},
				})
				wk.register({
					["]"] = {
						["<space>"] = { "Add new lines above and below cursor" },
					},
					["<leader>"] = {
						s = { "Strip unwanted whitespace" },
						z = { "Recursively unfold" },
						y = { "Copy into system clipboard" },
						p = { "Paste from system clipboard" },
						q = { "Quit" },
						f = {
              name = "Browse",
              l = { "Open file browser" },
              b = { "Find open buffers" },
              f = { "Find files in current directory" },
              g = { "Search across files" },
              h = { "Search help tags" }
            },
					},
          d = {
            b = { "Pick a buffer to delete" },
            s = { "Trigger surround-delete using motion" }
          },
					g = {
						c = { "Trigger commenting with motion" },
						h = { "Trigger hover preview from LSP" },
						r = { "Show all references of selected symbol" },
						y = { "Show type definition" },
            b = { "Pick a buffer to switch to" }
					},
					y = {
						s = { "Trigger surrounding with motion" },
					},
				})
			end,
		})

		-- indent guides
		use({
			"lukas-reineke/indent-blankline.nvim",
			config = function()
				vim.g["indent_blankline_filetype_exclude"] = { "terminal", "startify", "packer", "alpha" }
				vim.g["indent_blankline_context_patterns"] = {
					"declaration",
					"expression",
					"pattern",
					"primary_expression",
					"statement",
					"switch_body",
					"method",
					"class",
					"function",
					"block",
					"arguments",
				}
				vim.g["indent_blankline_show_current_context"] = 1
				vim.cmd("highlight IndentBlanklineContextChar guifg=#aaaaaa gui=nocombine")
			end,
		})

		-- file manager
		--[[ use({
			"ptzz/lf.vim",
			requires = "voldikss/vim-floaterm",
			config = function()
				vim.g["lf_map_keys"] = 0
				vim.api.nvim_set_keymap("n", "<Space>fl", ":LfWorkingDirectory<CR>", { silent = true })
			end,
		}) ]]
    use({
      "mroavi/lf.vim",
      config = function ()
        vim.g["lf#set_default_mappings"] = 0
        vim.g["lf#layout"] = {
          window = {
            width = 0.9,
            height = 0.8,
            highlight = "Debug"
          }
        }
        vim.api.nvim_set_keymap("n", "<leader>fl", ":LfPicker<CR>", {silent=true, noremap=true})
      end
    })

		---------
		-- STATUS
		---------
		-- scrollbar
		use("dstein64/nvim-scrollview")

		-- git gutter
		use({
			"lewis6991/gitsigns.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("gitsigns").setup()
			end,
		})

		-- todo comments
		use({
			"folke/todo-comments.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("todo-comments").setup({
					signs = true,
				})
			end,
		})

		-- colored icons
		use("kyazdani42/nvim-web-devicons")

		-- improved tabline
		use({
			"akinsho/bufferline.nvim",
			config = function()
				require("bufferline").setup({
					options = {
						numbers = "buffer_id",
						max_name_length = 24,
						enforce_regular_tabs = false,
						show_tab_indicators = true,
						show_buffer_close_icons = false,
						show_close_icon = false,
					},
				})
				vim.api.nvim_set_keymap("n", "gb", ":BufferLinePick<CR>", { silent = true })
				vim.api.nvim_set_keymap("n", "db", ":BufferLinePickClose<CR>", { silent = true })
			end,
		})

		-- statusline
		use({
			"nvim-lualine/lualine.nvim",
			requires = { "kyazdani42/nvim-web-devicons", opt = true },
			config = function()
				require("lualine").setup({
					options = {
						theme = "auto",
						component_separators = "",
						section_separators = "",
					},
					sections = {
						lualine_a = { { "mode" } },
						lualine_b = { { "branch" } },
						lualine_c = {
							{ "filename" },
							"g:coc_status",
						},
						lualine_x = { { "filetype", colored = false } },
						lualine_y = {
							{
								"diagnostics",
								sources = { "coc" },
								diagnostics_color = {
									error = { fg = "#d1666a" },
									warn = { fg = "#d1cd66" },
									hint = { fg = "#30d974" },
									info = { fg = "#307fd9" },
								},
								--[[ symbols = {
									error = " ",
									warn = " ",
									info = " ",
									hint = " ",
								}, ]]
							},
						},
						lualine_z = { { "location" } },
					},
				})
			end,
		})

		-----------------------
		-- SYNTAX SUPPORT & LSP
		-----------------------
		use("p00f/nvim-ts-rainbow") -- rainbow brackets
		use("JoosepAlviste/nvim-ts-context-commentstring") -- automatic commentstring based on syntax
		use("nvim-treesitter/nvim-treesitter-textobjects") -- custom textobjects with treesitter
		use("andymass/vim-matchup") -- extend "%" pair matching
		use({
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			config = function()
        vim.cmd("let g:matchup_matchparen_offscreen = {'method': 'popup'}")
				require("nvim-treesitter.configs").setup({
          ensure_installed = {
            "bash", "c", "css", "dart", "dockerfile",
            "go", "html", "javascript", "jsdoc", "json",
            "jsonc", "lua", "python", "regex", "rust",
            "scss", "svelte", "toml", "tsx", "typescript",
            "vim", "vue", "yaml"
          },
					highlight = {
						enable = true,
					},
					rainbow = {
						enable = true,
					},
					context_commentstring = {
						enable = true,
						enable_autocmd = false,
					},
					matchup = {
						enable = true,
					},
					textobjects = {
						select = {
							enable = true,
							lookahead = true,
							keymaps = {
								["af"] = "@function.outer",
								["if"] = "@function.inner",
								["ac"] = "@class.outer",
								["ic"] = "@class.inner",
							},
						},
					},
				})
			end
		})

		--- LSP CONFIGS
		use({ "neoclide/coc.nvim", branch = "release" })
		use({ "fannheyward/telescope-coc.nvim" })

		-- use({ "neovim/nvim-lspconfig" })
		-- use({ "onsails/lspkind-nvim" }) -- vscode symbols
		-- use("kabouzeid/nvim-lspinstall") -- easy installers
		-- use("nvim-lua/lsp-status.nvim") -- lsp messages
		-- use({ "hrsh7th/nvim-cmp" })
		-- use({ "hrsh7th/cmp-nvim-lsp" })
		-- use({ "hrsh7th/cmp-buffer" })
		-- use({ "hrsh7th/cmp-vsnip" })
		-- use({ "hrsh7th/vim-vsnip" })
		-- use({ "hrsh7th/cmp-path" })

		-- External services
		--[[ use({
			"akinsho/flutter-tools.nvim",
			requires = "nvim-lua/plenary.nvim",
		}) ]]

		-- Snippets
		use({
			"dsznajder/vscode-es7-javascript-react-snippets",
			run = "yarn install --frozen-lockfile && yarn compile",
		})
		use({ "Nash0x7E2/awesome-flutter-snippets" })
	end)
end

return M
