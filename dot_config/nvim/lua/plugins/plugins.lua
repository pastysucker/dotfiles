local M = {}

function M.setup()
  -- Automatically install packer
  local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
  if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = vim.fn.system({
      "git",
      "clone",
      "--depth",
      "1",
      "https://github.com/wbthomason/packer.nvim",
      install_path,
    })
    print("Installing packer close and reopen Neovim...")
    vim.cmd([[packadd packer.nvim]])
  end

  -- Autocommand that reloads neovim whenever you save the plugins.lua file
  -- vim.cmd([[
  --    augroup packer_user_config
  --      autocmd!
  --      autocmd BufWritePost plugins.lua source <afile> | PackerSync
  --    augroup end
  --  ]])

  -- Use a protected call so we don't error out on first use
  local status_ok, packer = pcall(require, "packer")
  if not status_ok then
    return
  end

  -- Have packer use a popup window
  packer.init({
    display = {
      open_fn = function()
        return require("packer.util").float({ border = "rounded" })
      end,
    },
    git = {
      clone_timeout = 120,
    },
  })

  -- Rocks
  require("packer.luarocks").install_commands()

  packer.startup(function(use)
    -- Packer managing itself
    use("wbthomason/packer.nvim")

    -- Rocks
    packer.use_rocks({ "lua-cjson", "http" })

    ---------------------
    -- Other dependencies
    ---------------------
    use({ "romgrk/fzy-lua-native" })

    -----
    -- UI
    -----
    use({ "rcarriga/nvim-notify" })
    use({ "stevearc/dressing.nvim" })

    ---------------
    -- Syntax/theme
    ---------------
    use({ "sainnhe/sonokai" })
    use({ "Mofiqul/dracula.nvim" })
    use({ "catppuccin/nvim", as = "catppuccin" })

    -- pug syntax
    use("digitaltoad/vim-pug")

    ---------
    -- Editor
    ---------
    -- highlight color literals like red, #cba22f
    use("NvChad/nvim-colorizer.lua")

    -- highlight rogue whitespaces
    use({
      "ntpeters/vim-better-whitespace",
      config = function()
        -- remove whitespaces when saving
        vim.cmd("autocmd BufWritePre * StripWhitespace")
      end,
    })

    -- start page
    use({
      "goolord/alpha-nvim",
      requires = { "kyazdani42/nvim-web-devicons" },
    })

    -- markdown view
    use({ "davidgranstrom/nvim-markdown-preview" })

    -- side panel doc
    use({ "amrbashir/nvim-docs-view", opt = true, cmd = { "DocsViewToggle" } })

    use("tpope/vim-repeat")

    -- range highlight
    use({ "winston0410/range-highlight.nvim", requires = "winston0410/cmd-parser.nvim" })

    -- type annotations
    use({
      "danymat/neogen",
      config = function()
        require("neogen").setup({})
      end,
      requires = "nvim-treesitter/nvim-treesitter",
      -- Uncomment next line if you want to follow only stable versions
      -- tag = "*"
    })

    ------------------------------------
    -- File/project finding & navigation
    ------------------------------------
    -- tmux integration
    use({ "sunaku/tmux-navigate" })

    -- terminal
    use({ "akinsho/toggleterm.nvim", branch = "main" })

    -- Multi purpose filter
    use({
      "nvim-telescope/telescope.nvim",
      requires = "nvim-lua/plenary.nvim",
    })
    use("nvim-telescope/telescope-fzy-native.nvim")

    -- Location list
    use({
      "folke/trouble.nvim",
      requires = "kyazdani42/nvim-web-devicons",
    })

    -- surround word
    use({
      -- "blackCauldron7/surround.nvim",
      -- "tpope/vim-surround",
      "kylechui/nvim-surround",
    })

    -- tabout from surrounds
    use({
      "abecodes/tabout.nvim",
      wants = { "nvim-treesitter" },
    })

    -- autopair
    use({
      "windwp/nvim-autopairs",
    })
    -- use({
    -- 	"ZhiyuanLck/smart-pairs",
    -- })

    -- commenting
    use({
      "numToStr/Comment.nvim",
    })

    -- find and replace
    use({
      "windwp/nvim-spectre",
      requires = "nvim-lua/plenary.nvim",
    })

    -- sneak plugin
    use({ "ggandor/leap.nvim" })

    -- popup keybindings
    use({
      "folke/which-key.nvim",
    })

    -- legend for your keymaps, commands, and autocmds, with which-key.nvim integration
    -- use({ "mrjones2014/legendary.nvim", })

    -- indent guides
    use({
      "lukas-reineke/indent-blankline.nvim",
    })

    -- function for auto indenting cursor
    use("winston0410/smart-cursor.nvim")

    -- file managers
    use({
      "mroavi/lf.vim",
    })
    use({
      "kyazdani42/nvim-tree.lua",
      requires = {
        "kyazdani42/nvim-web-devicons", -- optional, for file icon
      },
    })

    -- delete buffer properly
    use({
      "moll/vim-bbye",
      config = function()
        vim.cmd("command! -bang -complete=buffer -nargs=? Bd Bdelete<bang> <args>")
      end,
    })

    -- autoclose and autorename html tag
    use({
      "windwp/nvim-ts-autotag",
      requires = "nvim-treesitter/nvim-treesitter",
    })

    use({
      "phaazon/mind.nvim",
      branch = "v2.1",
      requires = { "nvim-lua/plenary.nvim" },
    })

    ---------
    -- STATUS
    ---------
    -- scrollbar
    use({
      "dstein64/nvim-scrollview",
      config = function()
        if vim.fn.exists("goneovim") == 1 then
          vim.cmd(":ScrollViewDisable")
        end
      end,
    })

    -- git gutter
    use({
      "lewis6991/gitsigns.nvim",
      requires = "nvim-lua/plenary.nvim",
    })

    -- neogit
    -- use({ "TimUntersberger/neogit" })

    -- vgit.nvim
    use({
      "tanvirtin/vgit.nvim",
      event = "BufWinEnter",
      requires = {
        "nvim-lua/plenary.nvim"
      }
    })

    -- todo comments
    use({
      "folke/todo-comments.nvim",
      requires = "nvim-lua/plenary.nvim",
    })

    -- colored icons
    use("kyazdani42/nvim-web-devicons")

    -- improved tabline
    use({
      "akinsho/bufferline.nvim",
      branch = "main",
    })

    -- statusline
    use({
      "nvim-lualine/lualine.nvim",
      requires = { "kyazdani42/nvim-web-devicons", opt = true },
    })

    -----------------------
    -- SYNTAX SUPPORT & LSP
    -----------------------
    use("watzon/vim-edge-template") -- adonis edge syntax
    use("nkrkv/nvim-treesitter-rescript") -- rescript support
    use("purescript-contrib/purescript-vim") -- purescript
    use("b0o/schemastore.nvim") -- json schemas

    use("p00f/nvim-ts-rainbow") -- rainbow brackets
    use("JoosepAlviste/nvim-ts-context-commentstring") -- automatic commentstring based on syntax
    use("nvim-treesitter/nvim-treesitter-textobjects") -- custom textobjects with treesitter
    use("andymass/vim-matchup") -- extend "%" pair matching
    use({
      "nvim-treesitter/nvim-treesitter",
      run = ":TSUpdate",
    })
    use("nvim-treesitter/playground")

    -- LSP bundles
    use({ "rescript-lang/vim-rescript" })
    use({ "akinsho/flutter-tools.nvim", requires = "nvim-lua/plenary.nvim" })
    use({ "jose-elias-alvarez/typescript.nvim", requires = "nvim-lua/plenary.nvim" })
    use({ "simrat39/rust-tools.nvim", requires = "nvim-lua/plenary.nvim" })
    use({ "ray-x/go.nvim" })
    use({ "p00f/clangd_extensions.nvim" })

    --- COC
    -- use({ "neoclide/coc.nvim", branch = "release" })
    -- use({ "fannheyward/telescope-coc.nvim" })

    -- NATIVE LSP
    use({
      "junnplus/nvim-lsp-setup",
      requires = {
        "neovim/nvim-lspconfig",
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
      },
    })
    -- use({ "onsails/lspkind-nvim" }) -- vscode symbols
    -- use("nvim-lua/lsp-status.nvim") -- lsp messages
    use({ "j-hui/fidget.nvim" }) -- lsp progress
    use({ "ray-x/lsp_signature.nvim" })
    use({ "hrsh7th/nvim-cmp" })
    use({ "hrsh7th/cmp-nvim-lsp" })
    use({ "hrsh7th/cmp-buffer" })
    use({ "hrsh7th/cmp-path" })
    use({ "hrsh7th/cmp-cmdline" })
    use({ "saadparwaiz1/cmp_luasnip" })
    use({ "L3MON4D3/LuaSnip" }) -- snippets
    use({ "tamago324/nlsp-settings.nvim" })
    use({ "jose-elias-alvarez/null-ls.nvim" }) -- additional functions like formatters and linters
  end)
end

return M
