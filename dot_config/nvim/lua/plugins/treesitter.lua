local function setup()
	local parser_config = require("nvim-treesitter.parsers").get_parser_configs()

	parser_config.rescript = {
		install_info = {
			url = "https://github.com/rescript-lang/tree-sitter-rescript",
			branch = "main",
			files = { "src/scanner.c" },
			generate_requires_npm = false,
			requires_generate_from_grammar = true,
      use_makefile = true
		}
	}
  parser_config.fsharp = {
    install_info = {
      url = 'https://github.com/ionide/tree-sitter-fsharp',
      branch = 'main',
      files = { 'src/scanner.c', 'src/parser.c' },
    },
    requires_generate_from_grammar = false,
    filetype = 'fsharp',
  }
	require("nvim-treesitter.configs").setup({
		ensure_installed = {
			"bash",
			"c",
			"css",
			"dart",
			"dockerfile",
			"go",
			"html",
			"javascript",
			"jsdoc",
			"json",
			"jsonc",
			"lua",
			"python",
			"regex",
			"rust",
			"scss",
			"svelte",
			"toml",
			"tsx",
			"typescript",
			"vim",
			"vue",
			"yaml",
		},
		highlight = {
			enable = true,
		},
		rainbow = {
			enable = true,
			colors = {
				"#ff5c57",
				"#f3f99d",
				"#5af78e",
				"#57c7ff",
				"#ff6ac1",
				"#9aedfe",
			},
		},
		matchup = {
			enable = true,
		},
		textobjects = {
			select = {
				enable = true,
				lookahead = true,
				keymaps = {
					["af"] = "@function.outer",
					["if"] = "@function.inner",
					["ac"] = "@class.outer",
					["ic"] = "@class.inner",
				},
			},
		},
		autotag = {
			enable = true,
			filetypes = {
				"html",
				"javascript",
				"javascriptreact",
				"typescriptreact",
				"svelte",
				"vue",
				"rescript",
			},
		},
		playground = {
			enable = true,
		},
	})
end

return { setup = setup }
