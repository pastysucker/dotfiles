local M = {}

function M.setup()
	-- Automatically install packer
	local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
	if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
		PACKER_BOOTSTRAP = vim.fn.system({
			"git",
			"clone",
			"--depth",
			"1",
			"https://github.com/wbthomason/packer.nvim",
			install_path,
		})
		print("Installing packer close and reopen Neovim...")
		vim.cmd("packadd packer.nvim")
	end

	-- Use a protected call so we don't error out on first use
	local status_ok, packer = pcall(require, "packer")
	if not status_ok then
		return
	end

	-- Have packer use a popup window
	packer.init({
		display = {
			open_fn = function()
				return require("packer.util").float({ border = "rounded" })
			end,
		},
		git = {
			clone_timeout = 120,
		},
	})

	-- Rocks
	require("packer.luarocks").install_commands()

	packer.startup(function(use)
		-- Packer managing itself
		use("wbthomason/packer.nvim")

		---------------------
		-- Other dependencies
		---------------------
		use("lewis6991/impatient.nvim")
		use({ "romgrk/fzy-lua-native" })

		-----
		-- UI
		-----
		use({ "rcarriga/nvim-notify", config = require("plugins.configs")["notify"] })
		use({ "stevearc/dressing.nvim" })

		---------------
		-- Syntax/theme
		---------------
		use({ "sainnhe/sonokai", config = require("plugins.configs")["sonokai"] })

		-- pug syntax
		use("digitaltoad/vim-pug")

		---------
		-- Editor
		---------
		-- highlight color literals like red, #cba22f
		use({ "NvChad/nvim-colorizer.lua", config = require("plugins.configs")["colorizer"] })

		-- highlight rogue whitespaces
		use({
			"ntpeters/vim-better-whitespace",
			config = require("plugins.configs")["vim-better-whitespace"],
		})

		-- start page
		use({
			"goolord/alpha-nvim",
			requires = { "kyazdani42/nvim-web-devicons" },
			config = require("plugins.configs")["alpha"],
		})

		-- markdown view
		use({ "davidgranstrom/nvim-markdown-preview" })

		-- side panel doc
		use({
			"amrbashir/nvim-docs-view",
			opt = true,
			cmd = { "DocsViewToggle" },
			config = require("plugins.configs")["docs-view"],
		})

		use("tpope/vim-repeat")

		-- range highlight
		use({
			"winston0410/range-highlight.nvim",
			requires = "winston0410/cmd-parser.nvim",
			config = require("plugins.configs")["range-highlight"],
		})

		-- type annotations
		use({
			"danymat/neogen",
			cmd = "Neogen",
			config = function()
				require("neogen").setup({})
			end,
			requires = "nvim-treesitter/nvim-treesitter",
			-- Uncomment next line if you want to follow only stable versions
			-- tag = "*"
		})

		-- better folding experience
		use({ "kevinhwang91/nvim-ufo", requires = "kevinhwang91/promise-async", event = "BufRead" })

		------------------------------------
		-- File/project finding & navigation
		------------------------------------
		-- tmux integration
		use({ "sunaku/tmux-navigate" })

		-- terminal
		use({ "akinsho/toggleterm.nvim", branch = "main", config = require("plugins.configs")["toggleterm"] })

		-- Multi purpose filter
		use({
			"nvim-telescope/telescope.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("plugins.configs").telescope()
			end,
		})
		use("nvim-telescope/telescope-fzy-native.nvim")

		-- Location list
		use({
			"folke/trouble.nvim",
			requires = "kyazdani42/nvim-web-devicons",
			config = require("plugins.configs")["trouble"],
		})

		-- surround word
		use({
			-- "blackCauldron7/surround.nvim",
			-- "tpope/vim-surround",
			"kylechui/nvim-surround",
			config = require("plugins.configs")["nvim-surround"],
		})

		-- tabout from surrounds
		use({
			"abecodes/tabout.nvim",
			wants = { "nvim-treesitter" },
			config = require("plugins.configs")["tabout"],
		})

		-- autopair
		use({
			"windwp/nvim-autopairs",
			config = require("plugins.configs")["nvim-autopairs"],
		})
		-- use({
		-- 	"ZhiyuanLck/smart-pairs",
		-- })

		-- commenting
		use({
			"numToStr/Comment.nvim",
			config = function()
				require("plugins.configs")["Comment"]()
			end,
		})

		-- find and replace
		use({
			"windwp/nvim-spectre",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("spectre").setup({})
				vim.api.nvim_create_user_command("Spectre", function()
					require("spectre").open()
				end, { nargs = 0 })
			end,
		})

		-- sneak plugin
		use({ "ggandor/leap.nvim", config = require("plugins.configs")["leap"] })

		-- popup keybindings
		use({
			"folke/which-key.nvim",
			config = function()
				require("plugins.configs")["which-key"]()
			end,
		})

		-- indent guides
		use({
			"lukas-reineke/indent-blankline.nvim",
			config = require("plugins.configs")["indent_blankline"],
		})

		-- function for auto indenting cursor
		use("winston0410/smart-cursor.nvim")

		-- file managers
		use({
			"mroavi/lf.vim",
			config = function()
				require("plugins.configs").lf()
			end,
		})

		use({
			"kyazdani42/nvim-tree.lua",
			requires = {
				"kyazdani42/nvim-web-devicons", -- optional, for file icon
			},
			config = require("plugins.configs")["nvim-tree"](),
		})

		-- delete buffer properly
		use({
			"moll/vim-bbye",
			config = function()
				vim.cmd("command! -bang -complete=buffer -nargs=? Bd Bdelete<bang> <args>")
			end,
		})

		-- autoclose and autorename html tag
		use({
			"windwp/nvim-ts-autotag",
			requires = "nvim-treesitter/nvim-treesitter",
		})

		use({
			"phaazon/mind.nvim",
			branch = "v2.1",
			requires = { "nvim-lua/plenary.nvim" },
			cmd = { "MindOpenMain", "MindOpenProject", "MindReloadState" },
			config = require("plugins.configs").mind,
		})

		---------
		-- STATUS
		---------
		-- scrollbar
		use({
			"dstein64/nvim-scrollview",
			config = function()
				if vim.fn.exists("goneovim") == 1 then
					vim.cmd(":ScrollViewDisable")
				end
			end,
		})

		-- git gutter
		use({
			"lewis6991/gitsigns.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = require("plugins.configs")["gitsigns"],
		})

		-- neogit
		-- use({ "TimUntersberger/neogit" })

		-- vgit.nvim
		use({
			"tanvirtin/vgit.nvim",
			cmd = "VGit",
			opt = true,
			requires = {
				"nvim-lua/plenary.nvim",
			},
			config = function()
				require("vgit").setup()
			end,
		})

		-- todo comments
		use({
			"folke/todo-comments.nvim",
			requires = "nvim-lua/plenary.nvim",
			config = require("plugins.configs")["todo-comments"],
		})

		-- colored icons
		use("kyazdani42/nvim-web-devicons")

		-- improved tabline
		use({
			"akinsho/bufferline.nvim",
			branch = "main",
			config = function()
				require("plugins.configs").bufferline()
			end,
		})

		-- lsp-progress
		use({
			"WhoIsSethDaniel/lualine-lsp-progress.nvim",
			requires = "nvim-lualine/lualine.nvim",
		})

		-- statusline
		use({
			"nvim-lualine/lualine.nvim",
			requires = { "kyazdani42/nvim-web-devicons", opt = true },
			config = function()
				require("plugins.configs").lualine()
			end,
		})

		-----------------------
		-- SYNTAX SUPPORT & LSP
		-----------------------
		use("watzon/vim-edge-template") -- adonis edge syntax
		use("nkrkv/nvim-treesitter-rescript") -- rescript support
		use("purescript-contrib/purescript-vim") -- purescript
		use("b0o/schemastore.nvim") -- json schemas

		use("p00f/nvim-ts-rainbow") -- rainbow brackets
		use("JoosepAlviste/nvim-ts-context-commentstring") -- automatic commentstring based on syntax
		use("nvim-treesitter/nvim-treesitter-textobjects") -- custom textobjects with treesitter
		use({
			"andymass/vim-matchup",
			event = "VimEnter",
			config = function()
				require("plugins.configs").matchup()
			end,
		}) -- extend "%" pair matching
		use({
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			config = function()
				require("plugins.configs").treesitter()
			end,
		})
		use("nvim-treesitter/playground")

		-- LSP bundles
		use({ "rescript-lang/vim-rescript", event = "BufRead", ft = "rescript" })
		use({ "akinsho/flutter-tools.nvim", requires = "nvim-lua/plenary.nvim" })
		use({ "jose-elias-alvarez/typescript.nvim", requires = "nvim-lua/plenary.nvim" })
		use({ "simrat39/rust-tools.nvim", requires = "nvim-lua/plenary.nvim" })
		use({ "ray-x/go.nvim" })
		use({ "p00f/clangd_extensions.nvim" })

		--- COC
		-- use({ "neoclide/coc.nvim", branch = "release" })
		-- use({ "fannheyward/telescope-coc.nvim" })

		use({ "L3MON4D3/LuaSnip", event = "BufRead" }) -- snippets
		use({ "rafamadriz/friendly-snippets" })

		-- NATIVE LSP
		use({
			"junnplus/nvim-lsp-setup",
			requires = {
				"neovim/nvim-lspconfig",
				"williamboman/mason.nvim",
				"williamboman/mason-lspconfig.nvim",
			},
		})
		use({ "j-hui/fidget.nvim", event = "VimEnter", disable = true }) -- lsp progress
		use({ "ray-x/lsp_signature.nvim", event = "VimEnter" })
		use({ "hrsh7th/nvim-cmp", event = { "VimEnter" } })
		use({ "hrsh7th/cmp-nvim-lsp", after = "nvim-cmp" })
		use({ "hrsh7th/cmp-buffer", after = "cmp-nvim-lsp" })
		use({ "hrsh7th/cmp-path", after = "cmp-buffer" })
		use({ "hrsh7th/cmp-cmdline", after = "cmp-path" })
		use({ "saadparwaiz1/cmp_luasnip", after = "cmp-cmdline" })
		use({ "hrsh7th/cmp-nvim-lua", after = "cmp_luasnip" })

		use({ "tamago324/nlsp-settings.nvim" })
		use({ "jose-elias-alvarez/null-ls.nvim" }) -- additional functions like formatters and linters
	end)
end

return M
