local utils = require("utils")
local k = utils["init-bindings"]()

local function setup()
	local telescope = require("telescope")
	local actions = require("telescope.actions")
	telescope.setup({
		defaults = {
			mappings = {
				i = {
					["<esc>"] = actions.close,
				},
			},
		},
		pickers = {
			lsp_references = {
				theme = "ivy",
			},
		},
	})
	--	telescope.load_extension("coc")
	telescope.load_extension("fzy_native")
  telescope.load_extension("flutter")
	-- vim.defer_fn(function()
	-- 	telescope.load_extension("notify")
	-- end, 500)

	-- bindings
	local telescope_builtin = require("telescope.builtin")
	local telescope_themes = require("telescope.themes")

	local set_map = utils["init-mapping-with-prefix"]("<leader>f", "n")
	set_map("f", function()
		telescope_builtin.find_files(telescope_themes.get_ivy())
	end, {
		desc = "Find files in current directory",
	})
	set_map("g", function()
		telescope_builtin.live_grep(telescope_themes.get_ivy())
	end, { desc = "Search across all files" })
	set_map("b", function()
		telescope_builtin.buffers(telescope_themes.get_dropdown())
	end, { desc = "Find open buffers" })
	set_map("h", function()
		telescope_builtin.help_tags()
	end, { desc = "Search for help tags" })

	k.nmap("<C-p>", function()
		telescope_builtin.find_files(telescope_themes.get_ivy())
	end, { desc = "Find files" })

  k.nmap("<C-S-p>", function ()
    require("commander").show()
  end)
end

return { setup = setup }
