local utils = require("utils")
local wk = require("which-key")

--[[
 NOTE: All LSP specific plugin settings are located in the "lsp" folder
]]

local M = {}

M["notify"] = function()
	vim.notify = require("notify")
end

M["sonokai"] = function()
	vim.g["sonokai_style"] = "andromeda"
	vim.g["sonokai_enable_italic"] = 1
	vim.g["sonokai_disable_italic_comment"] = 0
	vim.g["sonokai_menu_selection_background"] = "green"
	vim.g["sonokai_diagnostic_text_highlight"] = 0
	vim.g["sonokai_diagnostic_virtual_text"] = "colored"
	vim.g["sonokai_better_performance"] = 1
	vim.cmd("colorscheme sonokai")
end

M["colorizer"] = function()
	-- blue, #222, limegreen
	require("colorizer").setup({
		filetypes = { "*", "!alpha" },
		buftypes = { "*", "!alpha" },
		user_default_options = {
			names = false,
			css_fn = true,
			RRGGBBAA = true,
			AARRGGBB = true,
		},
	})
end

M["vim-better-whitespace"] = function()
	vim.cmd("autocmd BufWritePre * StripWhitespace")
	vim.g.better_whitespace_filetypes_blacklist = {
		"diff",
		"git",
		"gitcommit",
		"help",
		"markdown",
		"fugitive",
		"toggleterm",
		"NeogitStatus",
	}
end

M["alpha"] = function()
	--[[ local function fetch_trivia()
		local uri = "https://opentdb.com/api.php?amount=1&category=9&difficulty=medium"
		local resp = utils.fetch_json(uri)
		local result = resp.results[1]
		local question = utils.unescape_entities(result.question)
		print(question)
		local choices = result.incorrect_answers
		table.insert(choices, result.correct_answer)
		choices = utils.shuffle_table(choices)
		-- local choices = utils.shuffle_table(
		-- 	vim.tbl_extend("keep", { result.correct_answer }, result.incorrect_answers)
		-- )
		for _, choice in pairs(choices) do
			choice = utils.unescape_entities(choice)
		end
		vim.ui.select(choices, { prompt = question }, function(choice)
			if choice == result.correct_answer then
				vim.notify("You got it right!", "info", { render = "minimal" })
			else
				vim.notify("Nope! The right answer is " .. result.correct_answer, "error", { render = "minimal" })
			end
		end)
	end ]]

	local startify = require("alpha.themes.startify")
	local config = startify.config

	local Job = require("plenary.job")
	local fortune, _ = Job
		:new({
			command = "fortune",
			args = { "-s" },
		})
		:sync()
	local cowsay, _ = Job
		:new({
			command = "cowsay",
			args = { table.concat(fortune, "\n") },
		})
		:sync()

	startify.section.header.val = cowsay
	require("alpha").setup(config)
end

M["docs-view"] = function()
	require("docs-view").setup({
		position = "right",
		width = 60,
	})
end

M["range-highlight"] = function()
	require("range-highlight").setup({})
end

M["toggleterm"] = function()
	require("toggleterm").setup({
		open_mapping = "<C-`>",
	})

	local Terminal = require("toggleterm.terminal").Terminal

	-- lazygit
	Lazygit = Terminal:new({ cmd = "lazygit", hidden = true, direction = "float" })
	vim.api.nvim_create_user_command("LazygitToggle", function()
		Lazygit:toggle()
	end, {})

	-- glow
	Glow = Terminal:new({ cmd = "glow %", hidden = true, direction = "vertical" })
	vim.api.nvim_create_user_command("Glow", function()
		Glow:toggle()
	end, {})
end

M["telescope"] = function()
	local telescope = require("telescope")
	local actions = require("telescope.actions")
	telescope.setup({
		defaults = {
			mappings = {
				i = {
					["<esc>"] = actions.close,
				},
			},
		},
		pickers = {
			lsp_references = {
				theme = "ivy",
			},
		},
	})
	--	telescope.load_extension("coc")
	telescope.load_extension("fzy_native")
	vim.defer_fn(function()
		telescope.load_extension("notify")
	end, 500)

	-- bindings
	local telescope_builtin = require("telescope.builtin")
	local telescope_themes = require("telescope.themes")

	local set_map = utils.init_mapping_with_prefix("<leader>f", "n")
	set_map("f", function()
		telescope_builtin.find_files(telescope_themes.get_ivy())
	end, {
		desc = "Find files in current directory",
	})
	set_map("g", function()
		telescope_builtin.live_grep(telescope_themes.get_ivy())
	end, { desc = "Search across all files" })
	set_map("b", function()
		telescope_builtin.buffers(telescope_themes.get_dropdown())
	end, { desc = "Find open buffers" })
	set_map("h", function()
		telescope_builtin.help_tags()
	end, { desc = "Search for help tags" })

	utils.nmap("<C-p>", function()
		telescope_builtin.find_files(telescope_themes.get_ivy())
	end, { desc = "Find files" })
end

M["trouble"] = function()
	require("trouble").setup({
		signs = {
			error = "DiagnosticSignError",
			warning = "DiagnosticSignWarn",
			information = "DiagnosticSignInfo",
			hint = "DiagnosticSignHint",
		},
		auto_close = true,
		use_diagnostic_signs = true,
	})
end

M["nvim-surround"] = function()
	require("nvim-surround").setup({})
end

M["tabout"] = function()
	require("tabout").setup({})
end

M["nvim-autopairs"] = function()
	require("nvim-autopairs").setup({})
end

M["Comment"] = function()
	require("Comment").setup({
		pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
	})

	utils.nmap("<C-_>", "<Plug>(comment_toggle_linewise_current)", { desc = "Comment whole line" })
	utils.nmap("<C-/>", "<Plug>(comment_toggle_linewise_current)", { desc = "Comment whole line" })

	utils.imap("<C-_>", "<Esc><Plug>(comment_toggle_linewise_current)<esc>", { desc = "Comment line" })
	utils.imap("<C-/>", "<Esc><Plug>(comment_toggle_linewise_current)<esc>", { desc = "Comment line" })

	utils.xmap("<C-_>", "<Plug>(comment_toggle_blockwise_visual)", { desc = "Comment block" })
	utils.xmap("<C-/>", "<Plug>(comment_toggle_blockwise_visual)", { desc = "Comment block" })
end

M["leap"] = function()
	require("leap").set_default_keymaps()
end

M["which-key"] = function()
	wk.setup({
		plugins = {
			spelling = {
				enabled = true,
			},
		},
	})
	wk.register({
		["<leader>"] = {
			f = {
				name = "Finding",
			},
		},
		g = {
			c = { "Trigger commenting with motion" },
			h = { "Trigger hover preview from LSP" },
			r = { "Show all references of selected symbol" },
			y = { "Show type definition" },
			b = { "Pick a buffer to switch to" },
			L = { "Show workspace diagnostics " },
		},
		y = {
			s = { "Trigger surrounding with motion" },
		},
	})
end

M["indent_blankline"] = function()
	require("indent_blankline").setup({
		filetype_exclude = {
			"terminal",
			"startify",
			"packer",
			"alpha",
			"NvimTree",
			"lspinfo",
			"Trouble",
			"lsp-installer",
			"coctree",
		},
		context_patterns = {
			"declaration",
			"expression",
			"pattern",
			"primary_expression",
			"statement",
			"switch_body",
			"method",
			"class",
			"function",
			"block",
			"arguments",
		},
		show_current_context = true,
		show_current_context_start = true,
	})
	vim.g["indent_blankline_context_patterns"] = {}
	vim.g["indent_blankline_show_current_context"] = 1

	vim.api.nvim_create_augroup("MyColors", { clear = true })
	vim.api.nvim_create_autocmd("ColorScheme", {
		group = "MyColors",
		callback = function()
			vim.api.nvim_set_hl(0, "IndentBlanklineChar", { fg = "#35474f", nocombine = true })
			vim.api.nvim_set_hl(0, "IndentBlanklineContextChar", { fg = "#cccccc", nocombine = true })
		end,
	})
end

M["lf"] = function()
	vim.g["lf#set_default_mappings"] = 0
	vim.g["lf#layout"] = {
		window = {
			width = 0.9,
			height = 0.8,
			highlight = "Debug",
		},
	}
	utils.nmap("<leader>fl", "<cmd>LfPicker<CR>", { silent = true, desc = "Open Lf file manager" })
end

M["nvim-tree"] = function()
	require("nvim-tree").setup({
		view = {
			hide_root_folder = false,
			signcolumn = "no",
		},
		hijack_cursor = true,
	})
	utils.nmap("<leader>e", "<cmd>NvimTreeToggle<cr>", { desc = "Toggle file browser panel" })
end

M["mind"] = function()
	require("mind").setup()
end

M["gitsigns"] = function()
	require("gitsigns").setup()
end

M["todo-comments"] = function()
	require("todo-comments").setup({
		signs = true,
		highlight = {
			exclude = { "md", "markdown" },
		},
	})
end

M["bufferline"] = function()
	local bufferline_opts = {
		options = {
			numbers = "none",
			tab_size = 16,
			max_name_length = 24,
			enforce_regular_tabs = false,
			show_tab_indicators = true,
			show_buffer_close_icons = false,
			show_close_icon = false,
			always_show_bufferline = false,
			--[[ diagnostic = "lsp",
      diagnostics_update_in_insert = false, ]]
			diagnostics_indicator = function(count, _, _, _)
				if count ~= nil then
					if count > 9 then
						return "(9+)"
					else
						return "(" .. tostring(count) .. ")"
					end
				end
			end,
			-- indicator_icon = " ",
			offsets = {
				{
					filetype = "NvimTree",
					text = "File Explorer",
					highlight = "BufferLineBackground",
					text_align = "center",
				},
			},
		},
	}

	require("bufferline").setup(bufferline_opts)

	utils.nmap("gB", "<cmd>BufferLinePick<cr>", { desc = "Pick a buffer to switch to", noremap = false })
	utils.nmap("dB", "<cmd>BufferLinePickClose<cr>", { desc = "Pick a buffer to delete", noremap = false })
end

M.lualine = function()
	local spinners = {
		"⣼",
		"⣹",
		"⢻",
		"⠿",
		"⡟",
		"⣏",
		"⣧",
		"⣶",
	}
	require("lualine").setup({
		options = {
			icons_enabled = true,
			theme = "auto",
			component_separators = "",
			section_separators = "",
			globalstatus = false,
		},
		extensions = {
			"nvim-tree",
			"toggleterm",
			"quickfix",
		},
		sections = {
			lualine_a = { { "mode" } },
			lualine_b = {
				{ "b:gitsigns_head", icon = "" }, -- branch
				-- {
				-- 	"diff",
				-- 	source = function()
				-- 		local gitsigns = vim.b.gitsigns_status_dict
				-- 		if gitsigns then
				-- 			return {
				-- 				added = gitsigns.added,
				-- 				modified = gitsigns.changed,
				-- 				removed = gitsigns.removed,
				-- 			}
				-- 		end
				-- 	end,
				-- 	symbols = {
				-- 		added = " ",
				-- 		modified = "柳",
				-- 		removed = " ",
				-- 	},
				-- },
			},
			lualine_c = {
				{ "filename" },
				{
					"lsp_progress",
					colors = {
						title = "#7e8294",
						message = "#7e8294",
						percentage = "#7e8294",
						use = true,
					},
					timer = { spinner = 500, lsp_client_name_enddelay = 500 },
					display_components = { "lsp_client_name", { "title", "percentage", "message" } },
					spinner_symbols = spinners,
				},
			},
			lualine_x = { { "filetype", colored = false } },
			lualine_y = {
				{
					"diagnostics",
					sources = { "nvim_diagnostic" },
					diagnostics_color = {
						error = "DiagnosticError",
						warn = "DiagnosticWarn",
						hint = "DiagnosticHint",
						info = "DiagnosticInfo",
					},
					update_in_insert = false,
					symbols = {
						error = utils.get_sign("DiagnosticSignError"),
						warn = utils.get_sign("DiagnosticSignWarn"),
						info = utils.get_sign("DiagnosticSignInfo"),
						hint = utils.get_sign("DiagnosticSignHint"),
					},
				},
			},
			lualine_z = { { "location" } },
		},
	})
end

M["matchup"] = function()
	vim.g["matchup_matchparen_offscreen"] = { method = "popup" }
end

M["treesitter"] = function()
	require("nvim-treesitter.configs").setup({
		ensure_installed = {
			"bash",
			"c",
			"css",
			"dart",
			"dockerfile",
			"go",
			"html",
			"javascript",
			"jsdoc",
			"json",
			"jsonc",
			"lua",
			"python",
			"regex",
			"rescript",
			"rust",
			"scss",
			"svelte",
			"toml",
			"tsx",
			"typescript",
			"vim",
			"vue",
			"yaml",
		},
		highlight = {
			enable = true,
		},
		rainbow = {
			enable = true,
			colors = {
				"#ff5c57",
				"#f3f99d",
				"#5af78e",
				"#57c7ff",
				"#ff6ac1",
				"#9aedfe",
			},
		},
		context_commentstring = {
			enable = true,
			enable_autocmd = false,
		},
		matchup = {
			enable = true,
		},
		textobjects = {
			select = {
				enable = true,
				lookahead = true,
				keymaps = {
					["af"] = "@function.outer",
					["if"] = "@function.inner",
					["ac"] = "@class.outer",
					["ic"] = "@class.inner",
				},
			},
		},
		autotag = {
			enable = true,
			filetypes = {
				"html",
				"javascript",
				"javascriptreact",
				"typescriptreact",
				"svelte",
				"vue",
				"rescript",
			},
		},
		playground = {
			enable = true,
		},
	})
end

return M
