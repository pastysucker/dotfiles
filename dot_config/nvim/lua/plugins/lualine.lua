local utils = require("utils")
local navic = require("nvim-navic")

local function set_navic_highlights()
	vim.api.nvim_set_hl(0, "NavicIconsFile", { link = "Blue" })
	vim.api.nvim_set_hl(0, "NavicIconsModule", { link = "CmpItemKindModule" })
	vim.api.nvim_set_hl(0, "NavicIconsNamespace", { link = "TSNamespace" })
	vim.api.nvim_set_hl(0, "NavicIconsPackage", { link = "TSNamespace" })
	vim.api.nvim_set_hl(0, "NavicIconsClass", { link = "CmpItemKindClass" })
	vim.api.nvim_set_hl(0, "NavicIconsMethod", { link = "CmpItemKindMethod" })
	vim.api.nvim_set_hl(0, "NavicIconsProperty", { link = "CmpItemKindProperty" })
	vim.api.nvim_set_hl(0, "NavicIconsField", { link = "CmpItemKindField" })
	vim.api.nvim_set_hl(0, "NavicIconsConstructor", { link = "CmpItemKindConstructor" })
	vim.api.nvim_set_hl(0, "NavicIconsEnum", { link = "CmpItemKindEnum" })
	vim.api.nvim_set_hl(0, "NavicIconsInterface", { link = "CmpItemKindInterface" })
	vim.api.nvim_set_hl(0, "NavicIconsFunction", { link = "CmpItemKindFunction" })
	vim.api.nvim_set_hl(0, "NavicIconsVariable", { link = "CmpItemKindVariable" })
	vim.api.nvim_set_hl(0, "NavicIconsConstant", { link = "CmpItemKindConstant" })
	vim.api.nvim_set_hl(0, "NavicIconsString", { link = "TSString" })
	vim.api.nvim_set_hl(0, "NavicIconsNumber", { link = "TSNumber" })
	vim.api.nvim_set_hl(0, "NavicIconsBoolean", { link = "TSBoolean" })
	vim.api.nvim_set_hl(0, "NavicIconsArray", { link = "Orange" })
	vim.api.nvim_set_hl(0, "NavicIconsObject", { link = "Orange" })
	vim.api.nvim_set_hl(0, "NavicIconsKey", { link = "CmpItemKindKeyword" })
	vim.api.nvim_set_hl(0, "NavicIconsNull", { link = "TSKeyword" })
	vim.api.nvim_set_hl(0, "NavicIconsEnumMember", { link = "CmpItemKindEnumMember" })
	vim.api.nvim_set_hl(0, "NavicIconsStruct", { link = "CmpItemKindStruct" })
	vim.api.nvim_set_hl(0, "NavicIconsEvent", { link = "CmpItemKindEvent" })
	vim.api.nvim_set_hl(0, "NavicIconsOperator", { link = "CmpItemKindOperator" })
	vim.api.nvim_set_hl(0, "NavicIconsTypeParameter", { link = "CmpItemKindTypeParameter" })
	vim.api.nvim_set_hl(0, "NavicText", { default = true, fg = "#cccccc" })
	vim.api.nvim_set_hl(0, "NavicSeparator", { link = "Grey" })
end

local function setup()
	vim.api.nvim_create_autocmd("ColorScheme", {
		callback = function()
			set_navic_highlights()
		end,
		group = "colorscheme-loaded",
		pattern = "*",
	})
	set_navic_highlights()

	navic.setup({
		highlight = true,
		separator = " 🢒 ",
		depth_limit = 0,
		icons = {
			File = " ",
			Module = " ",
			Namespace = " ",
			Package = " ",
			Class = " ",
			Method = " ",
			Property = " ",
			Field = " ",
			Constructor = " ",
			Enum = " ",
			Interface = " ",
			Function = " ",
			Variable = " ",
			Constant = " ",
			String = " ",
			Number = " ",
			Boolean = " ",
			Array = " ",
			Object = " ",
			Key = " ",
			Null = " ",
			EnumMember = " ",
			Struct = " ",
			Event = " ",
			Operator = " ",
			TypeParameter = " ",
		},
	})

	local spinners = {
		"⣼",
		"⣹",
		"⢻",
		"⠿",
		"⡟",
		"⣏",
		"⣧",
		"⣶",
	}
	require("lualine").setup({
		options = {
			icons_enabled = true,
			theme = "auto",
			component_separators = "",
			section_separators = "",
			globalstatus = true,
		},
		extensions = {
			"nvim-tree",
			"toggleterm",
			"quickfix",
		},
		sections = {
			lualine_a = { { "mode" } },
			lualine_b = {
				{
					"b:gitsigns_head",
					icon = "שׂ",
					on_click = function()
						Lazygit:toggle()
					end,
				}, -- branch
				-- {
				-- 	"diff",
				-- 	source = function()
				-- 		local gitsigns = vim.b.gitsigns_status_dict
				-- 		if gitsigns then
				-- 			return {
				-- 				added = gitsigns.added,
				-- 				modified = gitsigns.changed,
				-- 				removed = gitsigns.removed,
				-- 			}
				-- 		end
				-- 	end,
				-- 	symbols = {
				-- 		added = " ",
				-- 		modified = "柳",
				-- 		removed = " ",
				-- 	},
				-- },
			},
			lualine_c = {
				{ "filename" },
				{
					"lsp_progress",
					colors = {
						title = "#7e8294",
						message = "#7e8294",
						percentage = "#7e8294",
						use = true,
					},
					timer = { spinner = 500, lsp_client_name_enddelay = 500 },
					display_components = { "lsp_client_name", { "title", "percentage", "message" } },
					spinner_symbols = spinners,
				},
			},
			lualine_x = {
				"copilot",
				{
					"filetype",
					colored = false,
					on_click = function()
						require("telescope.builtin").filetypes()
					end,
				},
			},
			lualine_y = {
				{
					"diagnostics",
					sources = { "nvim_diagnostic" },
					diagnostics_color = {
						error = "DiagnosticError",
						warn = "DiagnosticWarn",
						hint = "DiagnosticHint",
						info = "DiagnosticInfo",
					},
					update_in_insert = false,
					symbols = {
						error = utils["get-sign"]("DiagnosticSignError"),
						warn = utils["get-sign"]("DiagnosticSignWarn"),
						info = utils["get-sign"]("DiagnosticSignInfo"),
						hint = utils["get-sign"]("DiagnosticSignHint"),
					},
					on_click = function()
						require("trouble").toggle()
					end,
				},
			},
			lualine_z = { { "location" } },
		},
		winbar = {
			lualine_z = {
				{
					function()
						return vim.g.flutter_tools_decorations.device.name
					end,
					cond = function()
						return vim.g.flutter_tools_decorations ~= nil
					end,
				},
			},
			lualine_c = {
				{
					function()
						return navic.get_location()
					end,
					cond = navic.is_available,
				},
			},
		},
		--[[ tabline = {
			lualine_a = {
				{
					"buffers",
					mode = 2,
					symbols = {
						alternate_file = "",
					},
				},
			},
			lualine_z = { "tabs" },
		}, ]]
	})

	-- vim.keymap.set("n", "gB", ":LualineBuffersJump! ")
end

return { setup = setup }
