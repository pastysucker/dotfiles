local M = {}

local opts = {noremap = true, silent = false}

local generic_opts = {
  insert_mode = opts,
  normal_mode = opts,
}

local mode_adapters = {
  insert_mode = "i",
  normal_mode = "n",
  term_mode = "t",
  visual_mode = "v",
  visual_block_mode = "x",
  command_mode = "c",
}

local mappings = {
  insert_mode = {
    ["<C-s>"] = "<ESC>:w<CR>",
    ["C-BS"] = "<C-W>",
    ["jk"] = "<Esc>",
    ["<M-j>"] = "<Esc>:m .+1<CR>==gi",
    ["<M-k>"] = "<Esc>:m .-2<CR>==gi",
  },
  normal_mode = {
    [";"] = ":",
    ["<leader>q"] = ":q<CR>",
    ["<leader>y"] = "\"+y",
    ["<leader>p"] = "\"+p",
    [" <CR>"] = {":noh<CR><CR>", {silent = true}},
    -- Buffers
    ["<C-Tab>"] = {":bnext<CR>", {silent=true}},
    ["<C-S-Tab>"] = {":bprevious<CR>", {silent=true}},
    ["<C-]>"] = {":bnext<CR>", {silent=true}},
    ["<C-[>"] = {":bprevious<CR>", {silent=true}},
    -- Resizing panes
    ["<S-h>"] = ":vertical resize -10<CR>",
    ["<S-n>"] = ":vertical resize 10<CR>",
    ["<S-t>"] = ":resize -10<CR>",
    ["<S-c>"] = ":resize +10<CR>",

    -- Recursive unfold
    ["<leader>z"] = "zczA"
  }
}

function M.set_keymaps(mode, key, val)
  local opt = generic_opts[mode] and generic_opts[mode] or opts
  if type(val) == "table" then
    opt = val[2]
    val = val[1]
  end
  vim.api.nvim_set_keymap(mode, key, val, opt)
end

function M.map(mode, keymaps)
  mode = mode_adapters[mode] and mode_adapters[mode] or mode
  for k, v in pairs(keymaps) do
    M.set_keymaps(mode, k, v)
  end
end

function M.setup()
  for mode, mapping in pairs(mappings) do
    M.map(mode, mapping)
  end
end

return M
