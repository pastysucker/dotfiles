local utils = require("utils")

local M = {}

local o = vim.o -- set
local bo = vim.bo -- buffer-local options, eq setlocal
local wo = vim.wo -- window-local options
local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup
local resolve = vim.fn.expand
---@diagnostic disable-next-line: deprecated
local unpack = table.unpack or unpack

function M.setup()
	if vim.fn.exists("+termguicolors") == 1 then
		vim.cmd([[
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    ]])
		o.termguicolors = true
	end

	-----
	-- UI
	-----
	vim.cmd("set nowildmenu")
	if vim.fn.has("nvim") == 1 then
		vim.fn.setenv("NVIM_TUI_ENABLE_TRUE_COLOR", 1)
	end

	local highlights = {
		{ "DiagnosticError", { ctermfg = 1, fg = "#d1666a" } },
		{ "DiagnosticWarn", { ctermfg = 3, fg = "#d1cd66" } },
		{ "DiagnosticInfo", { ctermfg = 12, fg = "#307fd9" } },
		{ "DiagnosticHint", { ctermfg = 2, fg = "#30d974" } },

		{ "DiagnosticUnderlineHint", { underline = true, underdashed = true, sp = "Grey" } },
	}

	autocmd("ColorScheme", {
		pattern = "*",
		callback = function()
			utils.set_highlights(highlights)
		end,
	})

	---------
	-- EDITOR
	---------

	-- relative numbers only in normal mode
	autocmd("InsertEnter", {
		pattern = "*",
		callback = function()
			o.relativenumber = false
		end,
	})
	autocmd("InsertLeave", {
		pattern = "*",
		callback = function()
			o.relativenumber = true
		end,
	})

	o.re = 0 -- use new highlighting engine
	o.encoding = "utf-8"
	o.guifont = "JetBrainsMono Nerd Font:h11"
	o.cmdheight = 1
	o.mouse = "nvi" -- use mouse for everything
	o.showmode = false -- do not show current mode
	o.laststatus = 2 -- always display statusline
	o.cursorline = true -- highlight current line
	o.ruler = true
	o.number = true -- show line numbers

	o.backspace = "indent,eol,start" -- backspace everywhere

	o.autoindent = true -- auto indent for newline
	o.smartindent = true
	o.tabstop = 2
	o.shiftwidth = 2
	o.expandtab = true
	wo.linebreak = true -- break lines on words instead of in middle
	wo.breakindent = true -- wrapping lines is indented

	o.hlsearch = true -- highlight search terms
	wo.list = true -- show whitespace

	o.autoread = true -- auto reload files on change

	wo.scrolloff = 5 -- minmum lines above and below cursor

	--[[
	raw [[
    silent !mkdir ~/.local/share/nvim/_backup/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/_temp/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/_undo/ > /dev/null 2>&1
    silent !mkdir ~/.local/share/nvim/spell/ > /dev/null 2>&1
  ]]
	-- backup and temp
	--]]

	o.backupdir = resolve("~/.local/share/nvim/_backup") -- backup directory
	o.directory = resolve("~/.local/share/nvim/_temp") -- swap directory
	o.spellfile = resolve("~/.local/share/nvim/spell/en_us.utf-8.add")

	-- persist undo history
	if vim.fn.has("persistent_undo") == 1 then
		o.undodir = resolve("~/.local/share/nvim/_undo")
		o.undofile = true
	end

	-- goto last editing position
	autocmd("BufReadPost", {
		callback = function()
			local row, column = unpack(vim.api.nvim_buf_get_mark(0, '"'))
			local buf_line_count = vim.api.nvim_buf_line_count(0)

			if row >= 1 and row <= buf_line_count then
				vim.api.nvim_win_set_cursor(0, { row, column })
			end
		end,
	})

	-- highlight on yank
	augroup("HighlightYank", {})
	autocmd("TextYankPost", {
		group = "HighlightYank",
		pattern = "*",
		callback = function()
			vim.highlight.on_yank({ higroup = "DiffChange", timeout = 200 })
		end,
	})

	----------------
	-- PANES/BUFFERS
	----------------
	o.splitright = true
	o.equalalways = false -- prevents splits from all auto-adjusting horizontally when one closes

	o.foldmethod = "indent" -- fold based on indentation
	o.foldlevel = 99
	o.foldenable = false -- don't open a file with folds, display the whole thing
	o.signcolumn = "yes" -- always show the signcolumn
	o.wrap = false

	-- set the title of the window to the filename
	o.title = true
	o.titlestring = "%f%( [%M]%)"
end

return M
