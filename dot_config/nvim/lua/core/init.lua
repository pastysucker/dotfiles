local M = {}

function M.setup()
  require("core.editor").setup()
  require("core.keymaps")
end

return M
