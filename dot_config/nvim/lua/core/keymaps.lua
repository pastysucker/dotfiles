local u = require("utils")

vim.g.mapleader = " "

u.map(";", ":")

------------------
-- File management
------------------
u.map("<C-s>", ":w<CR>")
u.imap("<C-s>", "<ESC>:w<CR>")

u.nmap("<leader>q", ":q<CR>", { desc = "Quit" })

u.nmap("<leader>y", '"+y', { desc = "Copy into system clipboard" })
u.nmap("<leader>p", '"+p', { desc = "Paste from system clipboard" })

vim.keymap.set({ "i", "c", "t" }, "<C-BS>", "<C-W>")

u.nmap("<CR>", ":noh<CR><CR>", { silent = true })

----------
-- Buffers
----------
u.nmap("<C-Tab>", ":bnext<CR>", { silent = true })
u.nmap("<C-S-Tab>", ":bprevious<CR>", { silent = true })

u.nmap("<C-h>", ":bprevious<CR>", { silent = true })
u.nmap("<C-l>", ":bnext<CR>", { silent = true })
u.imap("<C-h>", "<Esc>:bprevious<CR>", { silent = true })
u.imap("<C-l>", "<Esc>:bnext<CR>", { silent = true })

u.nmap("QQ", ":Bdelete<CR>", { silent = true })
u.nmap("Qq", ":%bdelete<CR>", { silent = true })

-----------------
-- Panes & splits
-----------------
u.nmap("<C-y>", ":terminal<CR>")

u.nmap("<S-t>", ":resize -10<CR>")
u.nmap("<S-c>", ":resize +10<CR>")

----------
-- Editing
----------
u.imap("jk", "<Esc>")

-- copying/pasting
u.nmap('"+p', [[:let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p]])
u.nmap('"*p', [[:let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p]])
u.xmap('"+y', [[y:call system("wl-copy", @")<cr>]])

-- recursive unfold
u.nmap("<leader>z", "zczA", { desc = "Recursive unfold" })

-- insert blank line above and below cursor
u.nmap(
	"]<Space>",
	[[o<CR><Up><Esc>:lua require("smart-cursor").indent_cursor()<cr>a]],
	{ silent = true, desc = "Surround line with padding" }
)

-- indent cursor
u.nmap(
	"<leader><Tab>",
	[[:lua require("smart-cursor").indent_cursor()<cr>a]],
	{ silent = true, desc = "Smart indent cursor" }
)

vim.api.nvim_create_autocmd("FileType", {
	pattern = { "help", "mind" },
	callback = function()
		u.nmap("q", ":q<CR>", { silent = true, buffer = true })
	end,
})

