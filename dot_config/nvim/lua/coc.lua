local wk = require("which-key")

vim.g["coc_global_extensions"] = {
	"@yaegassy/coc-volar",
	"coc-css",
	"coc-deno",
	"coc-diagnostic",
	"coc-eslint",
	"coc-flutter-tools",
	"coc-go",
	"coc-html",
	"coc-json",
	"coc-lua",
	"coc-prettier",
	"coc-pyright",
	"coc-rust-analyzer",
	"coc-sh",
	"coc-snippets",
	"coc-stylelintplus",
	"coc-svelte",
	"coc-tailwindcss",
	"coc-tsserver",
	"coc-vimlsp",
	"coc-yaml",
	"https://github.com/Nash0x7E2/awesome-flutter-snippets",
	"https://github.com/dsznajder/vscode-es7-javascript-react-snippets",
}

wk.register({
	["<leader>c"] = { "<cmd>CocList commands<CR>" },
	["gd"] = { "<Plug>(coc-definition)" },
	["gy"] = { "<cmd>Telescope coc type_definitions theme=ivy<CR>" },
	["gi"] = { "<cmd>Telescope coc implementations theme=ivy<CR>" },
	["gr"] = { "<cmd>Telescope coc references theme=ivy<CR>" },
	["gh"] = { "<cmd>call CocActionAsync('doHover')<CR>" },
	["gL"] = { "<cmd>CocList diagnostics<CR>" },
	["<leader>gc"] = { "<Plug>(coc-codelens-action)" },
	["[d"] = { "<plug>(coc-diagnostic-prev)" },
	["]d"] = { "<plug>(coc-diagnostic-next)" },
	["<leader>F"] = { "<plug>(coc-format)" },
	["<C-."] = { "<plug>(coc-codeaction-selected" },
	["<F2>"] = { "<plug>(coc-rename)" },
	-- scroll floating window
	["<C-f>"] = {
		function()
			if vim.fn["coc#float#has_scroll"]() == 1 then
				vim.fn["coc#float#scroll"](1)
			else
				return "<C-f>"
			end
		end,
		nowait = true,
		expr = true,
	},
	["<C-b>"] = {
		function()
			if vim.fn["coc#float#has_scroll"]() == 1 then
				vim.fn["coc#float#scroll"](0)
			else
				return "<C-b>"
			end
		end,
		nowait = true,
		expr = true,
	},
})

wk.register({
	["<C-Space>"] = { "coc#refresh()", expr = true },
	-- scroll floating window
	["<C-f>"] = {
		function()
			if vim.fn["coc#float#has_scroll"]() == 1 then
				return "<C-r>=coc#float#scroll(1)<CR>"
			else
				return "<Right>"
			end
		end,
		nowait = true,
		expr = true,
	},
	["<C-b>"] = {
		function()
			if vim.fn["coc#float#has_scroll"]() == 1 then
				return "<C-r>=coc#float#scroll(0)<CR>"
			else
				return "<Left>"
			end
		end,
		nowait = true,
		expr = true,
	},
	-- suggestion UX navigation
	["<Tab>"] = {
		function()
			if vim.fn.pumvisible() == 1 then
				vim.fn["coc#_select_confirm"]()
			elseif vim.fn["coc#expandableOrJumpable"]() then
				return "<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])<CR>"
			elseif _G.check_back_space() then
				return "<Tab>"
			else
				return "<Plug>(Tabout)"
			end
		end,
		"Tab behaviour",
		expr = true,
	},
	["<S-Tab>"] = {
		[[pumvisible() ? "\<C-p>" : "\<S-Tab>"]],
		expr = true,
	},
	["<CR>"] = {
		[[pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]],
		expr = true,
	},
}, { mode = "i" })

local mappings = {
	normal = {},
	insert = {},
	visual = {
		["<C-."] = { "<plug>(coc-codeaction-selected", { silent = true } },
		["<leader>F"] = { "<plug>(coc-format-selected)" },
	},
}

for shortcut, conf in pairs(mappings.normal) do
	vim.api.nvim_set_keymap("n", shortcut, conf[1], conf[2] or {})
end

function _G.check_back_space()
	local col = vim.api.nvim_win_get_cursor(0)[2]
	return (col == 0 or vim.api.nvim_get_current_line():sub(col, col):match("%s")) and true
end

function _G.show_documentation()
	---@diagnostic disable-next-line: undefined-field
	if table.contains({ "vim", "help" }, vim.bo.filetype) then
		vim.api.nvim_command("h" .. vim.fn.expand("<cword>"))
	elseif vim.fn["coc#rpc#ready"]() then
		vim.fn.CocActionAsync("doHover")
	else
		vim.api.nvim_command("!" .. vim.o.keywordprg .. " " .. vim.fn.expand("<cword>"))
	end
end

function _G.go_to_definition()
	if vim.fn.CocAction("jumpDefinition") then
		return
	else
		vim.fn.searchdecl(vim.fn.expand("<cword>"))
	end
end
