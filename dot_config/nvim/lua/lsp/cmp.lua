local M = {}

local cmp_status_ok, cmp = pcall(require, "cmp")
if not cmp_status_ok then
	vim.api.nvim_err_writeln("cmp not found")
	return
end

local snip_status_ok, luasnip = pcall(require, "luasnip")
if not snip_status_ok then
	vim.api.nvim_err_writeln("luasnip not found")
	return
end

local check_backspace = function()
	local col = vim.fn.col(".") - 1
	return col == 0 or vim.fn.getline("."):sub(col, col):match("%s")
end

local html_tags = require("html_tags")

-- local has_words_before = function()
-- 	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
-- 	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
-- end
--
--   פּ ﯟ   some other good icons
local kind_icons = {
	Text = "  ",
	Method = "  ",
	Function = "  ",
	Constructor = "  ",
	Field = "  ",
	Variable = "  ",
	Class = "  ",
	Interface = "  ",
	Module = "  ",
	Property = "  ",
	Unit = "  ",
	Value = "  ",
	Enum = "  ",
	Keyword = "  ",
	Snippet = "  ",
	Color = "  ",
	File = "  ",
	Reference = "  ",
	Folder = "  ",
	EnumMember = "  ",
	Constant = "  ",
	Struct = "  ",
	Event = "  ",
	Operator = "  ",
	TypeParameter = "  ",

	--[[
	Text = "",
	Method = "m",
	Function = "",
	Constructor = "",
	Field = "",
	Variable = "",
	Class = "",
	Interface = "",
	Module = "",
	Property = "",
	Unit = "",
	Value = "",
	Enum = "",
	Keyword = "",
	Snippet = "",
	Color = "",
	File = "",
	Reference = "",
	Folder = "",
	EnumMember = "",
	Constant = "",
	Struct = "",
	Event = "",
	Operator = "",
	TypeParameter = "",
  --]]
}
-- find more here: https://www.nerdfonts.com/cheat-sheet

--- Custom captures are located at $NVIM_CONFIG/after/queries
---@param lang string
---@param query_string string
---@param capture_name string
---@return boolean
local function in_custom_ts_capture(lang, query_string, capture_name)
	local parser = vim.treesitter.get_parser(0, lang)
	local query = vim.treesitter.query.parse_query(lang, query_string)
	local tree = parser:parse()[1]
	---@diagnostic disable-next-line: deprecated
	local row = unpack(vim.api.nvim_win_get_cursor(0))
	local caps = {}
	for id, _, _ in query:iter_captures(tree:root(), 0, row - 1, row) do
		local name = query.captures[id]
		table.insert(caps, name)
	end
	return vim.tbl_contains(caps, capture_name)
end

local function setup_cmp_buffer()
	require("luasnip.loaders.from_vscode").lazy_load({ paths = "~/.config/nvim/snippets" })
	require("luasnip.loaders.from_vscode").lazy_load({
		paths = "~/.local/share/nvim/lazy/friendly-snippets",
		exclude = {
			"dart",
			"rescript",
			"javascript",
			"javascriptreact",
			"typescript",
			"typescriptreact",
		},
	})
	require("luasnip").filetype_extend("tera", { "htmldjango" })

	cmp.setup({
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body) -- For `luasnip` users.
			end,
		},
		mapping = {
			["<up>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
			["<down>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
			["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
			["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
			["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
			["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
			["<C-e>"] = cmp.mapping({
				i = cmp.mapping.abort(),
				c = cmp.mapping.close(),
			}),
			-- Accept currently selected item. If none selected, `select` first item.
			-- Set `select` to `false` to only confirm explicitly selected items.
			["<CR>"] = cmp.mapping({
				c = function(fallback)
					if cmp.visible() then
						cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
					else
						fallback()
					end
				end,
				i = cmp.mapping.confirm({
					select = false,
					behavior = cmp.ConfirmBehavior.Insert,
				}),
			}),
			["<Tab>"] = cmp.mapping({
				c = function()
					if cmp.visible() then
						cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
					else
						cmp.complete()
					end
				end,
				i = function(fallback)
					if require("copilot.suggestion").is_visible() then
            require("copilot.suggestion").accept()
						return fallback()
					end
					if cmp.visible() then
						cmp.confirm({ select = true })
					elseif luasnip.expand_or_locally_jumpable() then
						luasnip.expand_or_jump()
					elseif check_backspace() then
						fallback()
					else
						fallback()
					end
				end,
				s = function(fallback)
					if luasnip.jumpable() then
						luasnip.jump()
					else
						fallback()
					end
				end,
			}),
			["<S-Tab>"] = cmp.mapping({
				c = function()
					if cmp.visible() then
						cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
					else
						cmp.complete()
					end
				end,
				i = function(fallback)
					if cmp.visible() then
						cmp.select_prev_item()
					elseif luasnip.jumpable(-1) then
						luasnip.jump(-1)
					else
						fallback()
					end
				end,
				s = function(fallback)
					if luasnip.jumpable() then
						luasnip.jump(-1)
					else
						fallback()
					end
				end,
			}),
			["<PageUp>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select, count = 5 }),
			["<PageDown>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select, count = 5 }),
		},
		formatting = {
			fields = { "kind", "abbr", "menu" },
			format = function(entry, vim_item)
				-- Kind icons
				vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
				-- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
				vim_item.menu = ({
					nvim_lsp = "[LSP]",
					luasnip = "[Snippet]",
					buffer = "[Buffer]",
					path = "[Path]",
				})[entry.source.name]
				return vim_item
			end,
		},
		sources = cmp.config.sources({
			{
				name = "nvim_lsp",
				entry_filter = function(entry, ctx)
					local kinds = require("cmp.types").lsp.CompletionItemKind
					if kinds[entry:get_kind()] == "Snippet" then
						local lsp = vim.split(entry.source:get_debug_name(), ":")[2]
						if lsp == "emmet_ls" then
							local line = ctx.cursor_line
							local words = vim.split(line, " ")
							local last_word = words[#words]
							if vim.startswith(last_word, "#") or vim.startswith(last_word, ".") then
								return true
							end
							local parsed_markup =
								entry.source.source.client.request_sync("emmet/parseMarkup", { text = last_word })
							if parsed_markup["result"] == nil then
								return false
							end
							local starting_tag = parsed_markup["result"]["data"]["children"][1]["name"]
							return vim.tbl_contains(html_tags, starting_tag)
							-- return not in_custom_ts_capture(ctx.filetype, "(script_element) @script", "script")
						end
					end
					return true
				end,
			},
			{ name = "luasnip" },
			{ name = "nvim_lua" },
		}, {
			{ name = "buffer", keyword_length = 3 },
			{ name = "path", keyword_length = 2 },
		}),
		confirm_opts = {
			behavior = cmp.ConfirmBehavior.Replace,
			select = false,
		},
		preselect = cmp.PreselectMode.Item,
		behavior = cmp.SelectBehavior.Select,
		completion = {
			completeopt = "menu,menuone,noinsert",
		},
		window = {
			-- documentation = {
			-- border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
			--},
		},
		experimental = {
			ghost_text = false,
			native_menu = false,
		},
		-- sorting = {
		-- 	comparators = {
		-- 		cmp.config.compare.exact,
		-- 		cmp.config.compare.offset,
		-- 		cmp.config.compare.recently_used,
		-- 		require("clangd_extensions.cmp_scores"),
		-- 		cmp.config.compare.kind,
		-- 		cmp.config.compare.sort_text,
		-- 		cmp.config.compare.length,
		-- 		cmp.config.compare.order,
		-- 	},
		-- },
	})
	local cmp_autopairs = require("nvim-autopairs.completion.cmp")
	cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))

	-- cmp.event:on("menu_opened", function()
	-- 	vim.b.copilot_suggestion_hidden = true
	-- end)
	-- cmp.event:on("menu_closed", function()
	-- 	vim.b.copilot_suggestion_hidden = false
	-- end)
end

M.setup = function()
	setup_cmp_buffer()

	cmp.setup.cmdline(":", {
		sources = {
			{ name = "cmdline", keyword_length = 3 },
			{ name = "path" },
		},
	})

	cmp.setup.cmdline("/", {
		sources = {
			{ name = "buffer" },
		},
	})
end

return M

