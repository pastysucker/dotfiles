local M = {}
local null_ls = require("null-ls")
local helpers = require("null-ls.helpers")
-- local cmd_resolver = require("null-ls.helpers.command_resolver")
local builtins = require("null-ls.builtins")
local handlers = require("lsp.handlers")

-- DOCUMENTATION
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting

--------------
-- CUSTOM -- |
-------------

-- FORMATTERS
local fantomasfmt = helpers.make_builtin({
	name = "fantomas",
	method = null_ls.methods.FORMATTING,
	filetypes = { "fsharp" },
	generator_opts = {
		command = "fantomas",
		args = { "--stdin" },
		to_stdin = true,
	},
	factory = helpers.formatter_factory,
})

local dprint = helpers.make_builtin({
	name = "dprint",
	method = null_ls.methods.FORMATTING,
	filetypes = { "markdown", "toml" },
	generator_opts = {
		command = "dprint",
		args = { "fmt", "--stdin", "$FILENAME" },
		to_stdin = true,
	},
	factory = helpers.formatter_factory,
})

-------------
-- SETUP -- |
------------
M.setup = function()
	null_ls.setup({
    on_attach = handlers.on_attach(),
		sources = {
			builtins.formatting.prettierd.with({
				prefer_local = "node_modules/.bin",
				filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact", "json" },
				condition = function(utils)
					return not utils.root_has_file({
						"dprint.json",
						".dprint.json",
						"deno.json",
						"deno.jsonc",
					})
				end,
			}),
			-- builtins.formatting.eslint.with({
			-- 	prefer_local = "node_modules/.bin",
			-- 	filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
			-- }),
      builtins.formatting.fnlfmt,
			builtins.formatting.shfmt,
			builtins.formatting.stylua,
			-- builtins.diagnostics.eslint_d.with({
			-- 	prefer_local = "node_modules/.bin",
			-- 	filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
			-- 	condition = function(utils)
			-- 		return utils.root_has_file({
			-- 			".eslintrc",
			-- 			".eslintrc.js",
			-- 			".eslintrc.cjs",
			-- 			".eslintrc.yaml",
			-- 			".eslintrc.yml",
			-- 			".eslintrc.json",
			-- 			"package.json",
			-- 		})
			-- 	end,
			-- }),
			builtins.diagnostics.shellcheck,
			builtins.diagnostics.stylelint,
			builtins.code_actions.shellcheck,
			dprint,
		},
	})

	vim.api.nvim_create_user_command("NullLsDisable", function(command)
		null_ls.disable(command.args)
		print("Trying to disable " .. command.args)
	end, { nargs = 1 })

	vim.api.nvim_create_user_command("NullLsEnable", function(command)
		null_ls.enable(command.args)
		print("Trying to enable " .. command.args)
	end, { nargs = 1 })
end

return M
