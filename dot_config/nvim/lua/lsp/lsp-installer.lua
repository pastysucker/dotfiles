local M = {}

local lspconfig_util = require("lspconfig.util")
local nlspsettings = require("nlspsettings")
local handlers = require("lsp.handlers")

local function with_handlers(opts)
	local handler_opts = {
		on_attach = handlers.on_attach(),
		capabilities = handlers.capabilities,
	}
	if opts then
		return vim.tbl_deep_extend("force", handler_opts, opts)
	else
		return handlers
	end
end

local function setup_servers()
	local tsserver_root_dir = lspconfig_util.root_pattern("package.json")
	local denols_root_dir = lspconfig_util.root_pattern("deno.json", "deno.jsonc")

	require("nvim-lsp-setup").setup({
		default_mappings = false,
		on_attach = handlers.on_attach(),
		capabilities = handlers.capabilities,
		servers = {
			clangd = require("nvim-lsp-setup.clangd_extensions").setup({ server = with_handlers() }),
			clojure_lsp = {},
			cssls = {},
			dartls = require("flutter-tools").setup({
				lsp = with_handlers(),
			}),
			denols = {
				on_attach = handlers.on_attach({ exclusive_formatting = true }),
				root_dir = function(fname)
					return not tsserver_root_dir(fname) and denols_root_dir(fname)
				end,
			},
			emmet_ls = {
				filetypes = {
					"html",
					"css",
					"sass",
					"scss",
					"less",
					"edge",
				},
			},
			fsautocomplete = {
				single_file_support = true,
			},
			gopls = require("go").setup({
				lsp_cfg = {
					capabilities = handlers.capabilities,
					on_attach = handlers.on_attach(),
				},
			}),
			html = {},
			jsonls = {
				filetypes = { "json", "jsonc", "json5" },
				on_attach = handlers.on_attach({
					exclusive_formatting = true,
				}),
				settings = {
					json = {
						schemas = require("schemastore").json.schemas(),
						validate = { enable = true },
					},
				},
			},
			phpactor = {},
			purescriptls = {},
			pyright = {},
			rescriptls = {
				cmd = {
					"/home/tamim/.local/share/nvim/mason/bin/rescript-lsp",
					"--stdio",
				},
			},
			rust_analyzer = require("nvim-lsp-setup.rust-tools").setup({
				server = {
					settings = {
						["rust-analyzer"] = {
							cargo = {
								loadOutDirsFromCheck = true,
							},
							procMacro = {
								enable = true,
							},
						},
					},
				},
			}),
			-- stylelint_lsp = {}, -- RAM shortages
			sumneko_lua = {
				on_attach = handlers.on_attach({
					disable_formatter = true,
				}),
			},
			svelte = {},
			-- tailwindcss = {}, -- temporarily disabled due to lack of RAM
			tsserver = require("typescript").setup({
				server = with_handlers({ root_dir = tsserver_root_dir }),
			}),
			vimls = {},
			volar = {
				--[[
        on_new_config = function (new_config, new_root_dir)
          new_config.init_options.typescript.serverPath = utils.get_typescript_server_path(new_root_dir)
        end
				--]]
			},
		},
	})
end

--[[
local enhance_server_opts = {
	["denols"] = function(opts)
		opts.root_dir = function(fname)
			return not tsserver_root_dir(fname) and denols_root_dir(fname)
		end
	end,
	["tsserver"] = function(opts)
		opts.root_dir = tsserver_root_dir
	end,
	["tailwindcss"] = function(opts)
		local tailwindcss_defaults = require("lspconfig.server_configurations.tailwindcss").default_config
		opts.init_options = {
			userLanguages = {
				fsharp = "html",
			},
		}
		opts.filetypes = vim.tbl_extend("force", tailwindcss_defaults.filetypes, { "fsharp" })
	end,
}
--]]

M.setup = function()
	nlspsettings.setup({
		config_home = vim.fn.stdpath("config") .. "/lua/lsp/nlsp-settings",
		local_settings_dir = ".nlsp-settings",
		local_settings_root_markers = { ".git" },
		append_default_schemas = true,
		loader = "json",
	})

	setup_servers()
end

return M
-- local fslsconfig = {
-- 	name = "fsls",
-- 	cmd = {
-- 		"dotnet",
-- 		"/home/tamim/.vscode-insiders/extensions/faldor20.fsharp-language-server-updated-0.1.82/src/FSharpLanguageServer/bin/Release/net6.0/publish/FSharpLanguageServer.dll",
-- 	},
-- 	filetypes = { "fsharp" },
-- 	root_dir = "/home/tamim/Programming/rbds-services",
-- 	autostart = true,
-- 	on_attach = handlers.on_attach,
-- 	capabilities = handlers.capabilities,
-- }
-- vim.lsp.start_client(fslsconfig)
