local util = require("vim.lsp.util")

local M = {}

---@class AttachArgs
---@field keymaps table<string, any>
---@field disable_formatter boolean
---@field exclusive_formatting boolean

-- Show diagnostics in a pop-up window on hover
_G.LspDiagnosticPopupHandler = function()
	local current_cursor = vim.api.nvim_win_get_cursor(0)
	local last_popup_cursor = vim.w.lsp_diagnostics_last_cursor or { nil, nil }

	-- Show the popup diagnostics window,
	-- but only once for the current cursor location (unless moved afterwards).
	if not (current_cursor[1] == last_popup_cursor[1] and current_cursor[2] == last_popup_cursor[2]) then
		vim.w.lsp_diagnostics_last_cursor = current_cursor
		vim.diagnostic.open_float(0, { scope = "cursor", focus = false })
	end
end

local float_opts = {
	focusable = false,
	style = "minimal",
	border = { " ", "", " ", " ", " ", "", " ", " " },
	-- border = "rounded",   -- rounded | none
	source = "always",
	header = "",
	prefix = "",
	max_width = 70,
	-- Customize how diagnostic message will be shown: show error code.
	format = function(diagnostic)
		-- See null-ls.nvim#632, neovim#17222 for how to pick up `code`
		local user_data
		user_data = diagnostic.user_data or {}
		user_data = user_data.lsp or user_data.null_ls or user_data
		local code = (diagnostic.symbol or diagnostic.code or user_data.symbol or user_data.code)
		if code then
			return string.format("%s (%s)", diagnostic.message, code)
		else
			return diagnostic.message
		end
	end,
}

local function hover(_, result, ctx, config)
	config = config or {}
	config.focus_id = ctx.method
	if not (result and result.contents) then
		return
	end
	local markdown_lines = vim.lsp.util.convert_input_to_markdown_lines(result.contents)
	markdown_lines = vim.lsp.util.trim_empty_lines(markdown_lines)
	if vim.tbl_isempty(markdown_lines) then
		return
	end
	return vim.lsp.util.open_floating_preview(markdown_lines, "markdown", config)
end

M.setup = function()
	-- LSP progress spinner
	if pcall(require, "fidget") then
		require("fidget").setup({
			text = {
				spinner = "dots",
			},
			window = {
				blend = 0,
			},
		})
	end

	-- diagnostic on cursor hold
	local reset_group = vim.api.nvim_create_augroup("reset_group", {})
	vim.api.nvim_create_autocmd({ "CursorHold" }, {
		callback = function()
			_G.LspDiagnosticPopupHandler()
		end,
		group = reset_group,
	})

	local signs = {
		{ name = "DiagnosticSignError", text = "" },
		{ name = "DiagnosticSignWarn", text = "" },
		{ name = "DiagnosticSignInfo", text = "" },
		{
			name = "DiagnosticSignHint",
			text = "", --[[""--]]
		},
	}

	for _, sign in ipairs(signs) do
		vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
	end

	local diagnostic_config = {
		virtual_text = {
			severity = {
				max = vim.diagnostic.severity.ERROR,
				min = vim.diagnostic.severity.WARN,
			},
			prefix = "",
		},
		signs = {
			active = signs,
		},
		update_in_insert = false,
		underline = true,
		severity_sort = true,
		float = float_opts,
	}

	vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(hover, float_opts)
	vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, float_opts)
	vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
		vim.lsp.diagnostic.on_publish_diagnostics,
		diagnostic_config
	)
end

local function lsp_highlight_document(client)
	-- Set autocommands conditional on server_capabilities
	if client.server_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
    augroup lsp_document_highlight
    autocmd! * <buffer>
    autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
    autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
    augroup END
    ]],
			false
		)
	end
end

---@param bufnr number
---@param opts AttachArgs
local function lsp_register_keymaps(client, bufnr, opts)
	local extra_keymaps = opts.keymaps or {}
	local exclusive_formatting = opts.exclusive_formatting or false
	local wk = require("which-key")

	local default_mappings = {
		["gD"] = { "<cmd>lua vim.lsp.buf.declaration()<CR>", "Go to declaration" },
		["<F12>"] = { "<cmd>Trouble lsp_definitions<CR>", "Jump to definition" },
		["gh"] = { "<cmd>lua vim.lsp.buf.hover()<CR>", "Show LSP hover information" },
		["gi"] = { "<cmd>Trouble lsp_implementations<CR>", "See LSP implementations" },
		["<F2>"] = { "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename symbol" },
		["gr"] = { "<cmd>Trouble lsp_references<CR>", "See references" },
		["<C-.>"] = { "<cmd>lua vim.lsp.buf.code_action()<CR>", "Display code actions" },
		["[d"] = { "<cmd>lua vim.diagnostic.goto_prev()<CR>", "Jump to previous diagnostic" },
		["gl"] = { "<cmd>lua vim.lsp.diagnostic.open_float()<CR>", "Show diagnostic under cursor" },
		["gL"] = { "<cmd>Trouble workspace_diagnostics<CR>", "Show all workspace diagnostics" },
		["]d"] = { "<cmd>lua vim.diagnostic.goto_next()<CR>", "Jump to next diagnostic" },
		["<leader>"] = {
			["F"] = {
				function()
					if exclusive_formatting then
						local params = util.make_formatting_params({})
						client.request("textDocument/formatting", params, nil, bufnr)
					else
						vim.lsp.buf.format({ async = true })
					end
				end,
				"Format current file",
			},
		},
	}

	local keymaps = vim.tbl_deep_extend("force", default_mappings, extra_keymaps)

	wk.register(keymaps, { mode = "n", buffer = bufnr })

	vim.cmd([[ command! Format execute 'lua vim.lsp.buf.format()' ]])
end

---@param arg fun(client, bufnr: number)|AttachArgs|nil
---@return fun(client, bufnr: number)
M.on_attach = function(arg)
	return function(client, bufnr)
		---@type AttachArgs
		local opts = { keymaps = {}, exclusive_formatting = false, disable_formatter = false }
		if type(arg) == "function" then
			opts = vim.tbl_deep_extend("keep", arg(client, bufnr), opts)
		else
			opts = vim.tbl_deep_extend("keep", arg or opts, opts)
		end

		if opts.disable_formatter then
			client.server_capabilities.documentFormattingProvider = false
		end

		require("lsp_signature").on_attach({
			handler_opts = float_opts,
			doc_lines = 0,
			floating_window = true,
			auto_close_after = 5,
			toggle_key = "<C-.>",
		})
		lsp_register_keymaps(client, bufnr, opts)
		lsp_highlight_document(client)
	end
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.foldingRange = {
	dynamicRegistration = false,
	lineFoldingOnly = true,
}

local cmp_nvim_lsp = require("cmp_nvim_lsp")
M.capabilities = cmp_nvim_lsp.update_capabilities(capabilities)

-- folding capability
require("ufo").setup()

return M
