local util = require("vim.lsp.util")
local M = {}

function M.register_denols_commands(client, bufnr)
  vim.api.nvim_buf_create_user_command(bufnr, "DenolsInit", function(--[[opts]])
    local nt_utils = require("nvim-tree.utils")

    local root_dir = client.config.root_dir
    local config_file = root_dir .. "/.nlsp-settings/denols.json"

    if nt_utils.file_exists(config_file) then
      print(config_file .. " already exists")
    else
      local dir_ok = vim.loop.fs_mkdir(".nlsp-settings", tonumber("0777", 8))
      if not dir_ok then
        vim.api.nvim_err_writeln(".nlsp-settings already exists!")
        return
      end
      local ok, fd = pcall(vim.loop.fs_open, config_file, "w", tonumber("0644", 8))
      if not ok or fd == nil then
        vim.api.nvim_err_writeln("Couldn't create LSP config file!")
        return
      end
      vim.loop.fs_write(
        fd,
        [[{
"deno.enable": true,
"deno.lint": true,
"deno.importMap": "./import_map.json",
"deno.config": "./deno.json"
}]]
      )
      vim.loop.fs_close(fd)
      vim.notify("denols.json file created!", vim.log.levels.INFO, {})
    end
  end, {})
end

M.with_exclusive_format_keymap = function(table)
  return function(client, bufnr)
    local keymap = {
      ["<leader>"] = {
        ["F"] = {
          function()
            local params = util.make_formatting_params({})
            client.request("textDocument/formatting", params, nil, bufnr)
          end,
          "Format current file",
        },
      },
    }
    if table then
      return vim.tbl_deep_extend("force", keymap, table)
    else
      return keymap
    end
  end
end

return M
