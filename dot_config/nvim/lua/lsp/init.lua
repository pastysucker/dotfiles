local M = {
  setup = function()
    require("lsp.lsp-installer").setup()
    require("lsp.handlers").setup()
    require("lsp.null-ls").setup()
    require("lsp.cmp").setup()
  end
}

return M
