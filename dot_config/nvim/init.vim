let g:loaded_netrw       = 1
let g:loaded_netrwPlugin = 1
""" Don't care about compatibility
" More info:
" https://stackoverflow.com/questions/5845557/in-a-vimrc-is-set-nocompatible-completely-useless
set nocompatible
" True Colors (tm)
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

set pumblend=10

""" Signs
call sign_define([
      \ {'name': "DiagnosticSignError", 'text': ""},
      \ {'name': "DiagnosticSignWarn", 'text': ""},
      \ {'name': "DiagnosticSignHint", 'text': ""},
      \ {'name': "DiagnosticSignInfo", 'text': ""}
      \ ])

let g:loaded_matchparen        = 1
let g:loaded_matchit           = 1
let g:loaded_logiPat           = 1
let g:loaded_rrhelper          = 1
let g:loaded_tarPlugin         = 1
" let g:loaded_man               = 1
let g:loaded_gzip              = 1
let g:loaded_zipPlugin         = 1
let g:loaded_2html_plugin      = 1
let g:loaded_shada_plugin      = 1
let g:loaded_spellfile_plugin  = 1
let g:loaded_netrw             = 1
let g:loaded_netrwPlugin       = 1
let g:loaded_tutor_mode_plugin = 1
let g:loaded_remote_plugins    = 1

""" REQUIRES
lua require("plugins.plugins").setup()
lua require("plugins.configs").setup()
lua require("lsp.init")

if exists("g:neovide")
  so ~/.config/nvim/neovide.vim
endif
if exists("g:nvui")
  so ~/.config/nvim/nvui.vim
endif

so ~/.config/nvim/editor.vim
so ~/.config/nvim/keymap.vim
" so ~/.config/nvim/coc.vim

set updatetime=700

""" Highlight definitions
hi DiagnosticError ctermfg=1 guifg=#d1666a
hi DiagnosticWarn ctermfg=3 guifg=#d1cd66
hi DiagnosticInfo ctermfg=12 guifg=#307fd9
hi DiagnosticHint ctermfg=2 guifg=#30d974
hi DiagnosticUnderlineHint cterm=underline gui=underdashed guisp=Grey

hi link DiagnosticSignError DiagnosticError
hi link DiagnosticSignWarn DiagnosticWarn
hi link DiagnosticSignInfo DiagnosticInfo
hi link DiagnosticSignHint DiagnosticHint

""" Autosave after 'updatetime'
" autocmd CursorHold * wa

""" Batch'd Setup / Config Scripts
" call EnableTemplateLiteralColors()
