if exists(":GuiFont")
  GuiFont! JetBrainsMono\ Nerd\ Font\ Mono:h11
endif

" Disable GUI Tabline
if exists(':GuiTabline')
  GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
  GuiPopupmenu 0
endif

if exists(":GuiRenderLigatures")
  GuiRenderLigatures 1
endif

" Right Click Context Menu (Copy-Cut-Paste)
nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
xnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv
snoremap <silent><RightMouse> <C-G>:call GuiShowContextMenu()<CR>gv

" Keybindings
map <S-Insert> <C-r>+
map! <S-Insert> <C-r>+

" Panes
nnoremap <A-CR> <cmd>vsplit<cr>
nnoremap <A-Up> <C-w><C-k>
nnoremap <A-Left> <C-w><C-h>
nnoremap <A-Down> <C-w><C-j>
nnoremap <A-Right> <C-w><C-l>
nnoremap <A-k> <C-w><C-k>
nnoremap <A-h> <C-w><C-h>
nnoremap <A-j> <C-w><C-j>
nnoremap <A-l> <C-w><C-l>

inoremap <A-CR> <esc><cmd>vsplit<cr>
inoremap <A-Up> <esc><C-w><C-k>
inoremap <A-Left> <esc><C-w><C-h>
inoremap <A-Down> <esc><C-w><C-j>
inoremap <A-Right> <esc><C-w><C-l>
inoremap <A-k> <esc><C-w><C-k>
inoremap <A-h> <esc><C-w><C-h>
inoremap <A-j> <esc><C-w><C-j>
inoremap <A-l> <esc><C-w><C-l>
