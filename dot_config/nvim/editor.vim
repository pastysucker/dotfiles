"""""""""""""""""""""""""
" UI / Aesthetics
"""""""""""""""""""""""""
let g:loaded_netrw = 1

if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

set nowildmenu

"""""""""""""""""""""""""
" Editor
"""""""""""""""""""""""""
syntax on
set encoding=utf-8
set guifont=JetBrainsMono\ Nerd\ Font:h11

set cmdheight=1      " under statusline messages

set mouse=a          " use mouse for everything
set noshowmode       " show the current mode (Insert, Visual...)
set laststatus=2     " Always display status line

set cursorline       " highlight current line

set ruler            " show current position

set number           " line numbers

" Relative line numbers only in normal mode
autocmd InsertEnter * :set norelativenumber
autocmd InsertLeave * :set relativenumber

set backspace=indent,eol,start  " backspace everywhere

set autoindent   " Use current indentation level for new lines
set smartindent  " Try to guess indentation based on previous line

" Default indentation - editorconfig should override these
set tabstop=2
set shiftwidth=2
set expandtab
set linebreak " breaks lines on words instead of in the middle of a word
set breakindent " wrapping of lines is indented
let &showbreak='⇢ ' " the wrapped part of a line is indented a bit

" turn off smart indentation when pasting
" set pastetoggle=<F2>

set hlsearch  " highlight search terms
set list      " show whitespace

" set whitespace chars
" set listchars=eol:¬,tab:>·,extends:>,precedes:<,space:·

set autoread   " Autoload reload files when they have changed on the disk

" Scrolling
set scrolloff=5  " minimum lines to keep above and below cursor

" Backup and Temp
silent !mkdir ~/.local/share/nvim/_backup/ > /dev/null 2>&1
silent !mkdir ~/.local/share/nvim/_temp/ > /dev/null 2>&1
silent !mkdir ~/.local/share/nvim/_undo/ > /dev/null 2>&1
silent !mkdir ~/.local/share/nvim/spell/ > /dev/null 2>&1

set backupdir=~/.local/share/nvim/_backup/    " where to put backup files.
set directory=~/.local/share/nvim/_temp/      " where to put swap files.

" allow undo history to persist after closing buffer
if has('persistent_undo')
  set undodir=~/.local/share/nvim/_undo
  set undofile
end

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif

" Highlight on yank, above 0.4.4
augroup highlight_yank
  autocmd!
  au TextYankPost * silent! lua vim.highlight.on_yank { higroup='MatchParen', timeout=200 }
augroup END


""""""""""""""
" Spell Checker
""""""""""""""
set spellfile=~/.local/share/nvim/spell/en_us.utf-8.add

""""""""""""""""""""
" Panes / Buffers
""""""""""""""""""""
set splitright
set equalalways noequalalways " prevents splits from all auto-adjusting horizontally when one closes

""""""""""""""""""""
" Code Management
""""""""""""""""""""
set foldmethod=indent "" fold based on indentation
set foldlevel=99
set nofoldenable      "" don't open a file with folds, display the whole thing
set signcolumn=yes    "" always show the signcolumn
set nowrap

"" set the title of the window to the filename
set title
set titlestring=%f%(\ [%M]%)
