let g:coc_global_extensions = [
      \ '@yaegassy/coc-volar',
      \ 'coc-clangd',
      \ 'coc-css',
      \ 'coc-deno',
      \ 'coc-diagnostic',
      \ 'coc-elixir',
      \ 'coc-eslint',
      \ 'coc-flutter-tools',
      \ 'coc-go',
      \ 'coc-html',
      \ 'coc-json',
      \ 'coc-lua',
      \ 'coc-prettier',
      \ 'coc-pyright',
      \ 'coc-rust-analyzer',
      \ 'coc-sh',
      \ 'coc-snippets',
      \ 'coc-stylelintplus',
      \ 'coc-svelte',
      \ 'coc-tailwindcss',
      \ 'coc-tsserver',
      \ 'coc-vimlsp',
      \ 'coc-yaml',
      \ 'https://github.com/Nash0x7E2/awesome-flutter-snippets',
      \ 'https://github.com/dsznajder/vscode-es7-javascript-react-snippets'
      \ ]

" Hotkeys
nnoremap <silent> <space>fc <cmd>Telescope coc commands theme=ivy<cr>
inoremap <silent> <expr> <c-space> coc#refresh()
inoremap <silent> <c-s-space> <C-r>=CocActionAsync("showSignatureHelp")<cr>
inoremap <silent> <C-.> <C-r>=CocActionAsync("showSignatureHelp")<cr>

" Remap keys for gotos
nmap <silent> <F12> :call <SID>GoToDefinition()<CR>
nmap <silent> <leader>gd <Plug>(coc-definition)
nmap <silent> gD <cmd>Trouble coc_definitions<cr>
nmap <silent> gy <cmd>Trouble coc_type_definitions<cr>
nmap <silent> gi <cmd>Trouble coc_implementations<cr>
nmap <silent> gr <cmd>Trouble coc_references<cr>
nmap <silent> gr <cmd>Trouble coc_references_used<cr>
nmap <F2> <Plug>(coc-rename)
nmap <silent> <leader>F <Plug>(coc-format)
vmap <expr> <leader>F <Plug>(coc-format-selected)
nmap <silent> [d <Plug>(coc-diagnostic-prev)
nmap <silent> ]d <Plug>(coc-diagnostic-next)
nmap <silent> gL <cmd>Trouble coc_workspace_diagnostics<CR>

"" Remap for code action
nnoremap <silent> gh <cmd>call CocActionAsync('doHover')<CR>
nmap <leader>gc <Plug>(coc-codelens-action)

"" Scroll floating window
if has('nvim-0.4.3') || has('patch-8.2.0750')
  nnoremap <nowait> <expr> <silent> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <nowait> <expr> <silent> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <nowait> <expr> <silent> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <nowait> <expr> <silent> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
endif

vmap <silent> <C-.>  <Plug>(coc-codeaction-selected)
" nmap <silent> <C-.>  v<Plug>(coc-codeaction-selected)
nmap <silent> <C-.> <cmd>CocAction<CR>

" Open
imap <silent> <expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" : "\<Plug>(Tabout)"

"""
" Use enter to confirm completion
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" To make <cr> select the first completion item and confirm the completion when no item has been selected:
" inoremap <silent> <expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
"""

" Format on carriage return
inoremap <silent> <expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

"" Close preview window when completion is done.
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
"" Highlight symbol when holding cursor
autocmd CursorHold * silent call CocActionAsync('highlight')
"" Show function signature when jumping placeholders
autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

hi link CocErrorVirtualText DiagnosticError
hi link CocWarningVirtualText DiagnosticWarn
hi link CocHintVirtualText DiagnosticHint
hi link CocInfoVirtualText DiagnosticInfo

hi link CocErrorSign DiagnosticSignError
hi link CocWarningSign DiagnosticSignWarn
hi link CocHintSign DiagnosticSignHint
hi link CocInfoSign DiagnosticSignInfo

"""""""""""
""" OPTIONS
"""""""""""
let g:coc_snippet_next = '<tab>'

"""""""""""""
""" FUNCTIONS
"""""""""""""
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Show documentation by pressing K
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Go to definition
" coc -> tags -> searchdecl
function! s:GoToDefinition()
  if CocAction('jumpDefinition')
    return v:true
  endif

  let ret = execute("silent! normal \<C-]>")
  if ret =~ "Error" || ret =~ "错误"
    call searchdecl(expand('<cword>'))
  endif
endfunction
