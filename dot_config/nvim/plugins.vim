call plug#begin('~/.local/share/nvim/plugged')
"""""""""""""""""""""
" Common dependencies
"""""""""""""""""""""
Plug 'nvim-lua/plenary.nvim'

"""""""""""""""""
" Syntax / Theme
" Plug 'joshdick/onedark.vim'
" Plug 'taniarascia/new-moon.vim'
Plug 'sainnhe/sonokai'

let g:sonokai_style = 'atlantis'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1

"""""""""""""""""
" Editor
"""""""""""""""""
Plug 'etdev/vim-hexcolor'
" Highlight trailing workspaces
Plug 'ntpeters/vim-better-whitespace'
autocmd BufWritePre * StripWhitespace

Plug 'mhinz/vim-startify'

""""""""""""""""""""""""""
""" File / Project Finding
""""""""""""""""""""""""""

""" fzf integration
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.6, 'border': 'rounded' } }
nmap <C-p> :Files<CR>
noremap <C-S-f> :Rg<CR>

""" Working with code
" Plug 'jiangmiao/auto-pairs'
" let g:AutoPairsFlyMode = 1
" M-b jumps back
let g:AutoPairsShortcutJump = ''
let g:AutoPairsShortcutFastWrap = ''

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

""" Sneak plugin
" Plug 'justinmk/vim-sneak'
Plug 'ggandor/lightspeed.nvim'

Plug 'editorconfig/editorconfig-vim'

""" Shows popup keybindings
Plug 'folke/which-key.nvim'

""" Indent guides
Plug 'lukas-reineke/indent-blankline.nvim'
let g:indent_blankline_filetype_exclude = ['terminal', 'startify', 'defx']
let g:indent_blankline_context_patterns = ['declaration', 'expression', 'pattern', 'primary_expression', 'statement', 'switch_body', 'method', 'class', 'function', 'block', 'arguments' ]
highlight IndentBlanklineContextChar guifg=#00FF00 gui=nocombine

let g:indent_blankline_show_current_context = 1

""" Colorize brackets
Plug 'luochen1990/rainbow'
let g:rainbow_active = 1
" Can be toggled with :RainbowToggle

""" Git Conflict highlighting
Plug 'tpope/vim-fugitive'

""" Tmux integration
Plug 'sunaku/tmux-navigate'

""" File explorer
" --- Lf integration
Plug 'ptzz/lf.vim'
let g:lf_map_keys = 0
map <leader>fl :LfWorkingDirectory<CR>
map <C-S-E>fl :LfWorkingDirectory<CR>

""""""""""""""""""""
" Language Servers
"
" Debugging:
"   node -e 'console.log(path.join(os.tmpdir(), "coc-nvim.log"))'
""""""""""""""""""""
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'dsznajder/vscode-es7-javascript-react-snippets', { 'do': 'yarn install --frozen-lockfile && yarn compile' }
Plug 'Nash0x7E2/awesome-flutter-snippets'

""""""""""""""""""
"" Status
""""""""""""""""""
Plug 'wfxr/minimap.vim', {'do': ':!cargo install --locked code-minimap'}
let g:minimap_width = 10
let g:minimap_auto_start = 0
let g:minimap_auto_start_win_enter = 1
" let g:minimap_highlight = 0
let g:minimap_highlight_range = 1
" let g:minimap_highlight_search = 0
" let g:minimap_git_colors = 0

""" Scrollbar
Plug 'dstein64/nvim-scrollview'

""" Show git file status in gutter
Plug 'lewis6991/gitsigns.nvim'

""" Comments
Plug 'folke/todo-comments.nvim'

""" Coloured icons
Plug 'kyazdani42/nvim-web-devicons'

""" Improved tabline (requires Nvim with Lua)
Plug 'akinsho/bufferline.nvim'

""" Lualine (faster statusline)
Plug 'shadmansaleh/lualine.nvim'

""" Vim Airline
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" Disable built in tabline for bufferline
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.dirty='⚡'

" Configuration
" A - display mode = flags
" B - version control
" C - file name
" X - file type
" Y - file encoding
" Z - current position
" ... addition stuff
" https://github.com/vim-airline/vim-airline#configurable-and-extensible
function! AirlineInit()
  let g:airline_section_a = airline#section#create(['mode'])
  let g:airline_section_b = airline#section#create_left(['branch'])
  let g:airline_section_c = airline#section#create(['%f'])
  let g:airline_section_x = airline#section#create([''])
  let g:airline_section_y = airline#section#create(['filetype'])
  let g:airline_section_z = airline#section#create(['%l:%c'])
endfunction
" autocmd VimEnter * call AirlineInit()


"""""""""""""""""""""
" Syntax Support

" Syntax not needed, because provided by polyglot

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate', 'branch': '0.5-compat'}

set re=0

" Polyglot Markdown provided by: https://github.com/plasticboy/vim-markdown
" let g:vim_markdown_fenced_languages = ['js=javascript', 'ts=typescript', 'hbs=html.handlebars', 'bash=sh']

" CSS
Plug 'alexlafroscia/postcss-syntax.vim'

" Nested syntax highlighting
" needed for js/ts named template literals
" and markdown.
Plug 'Quramy/vim-js-pretty-template'

""" Floaterm
Plug 'voldikss/vim-floaterm'

call plug#end()


"""""""""""
" LUA SETUP
"""""""""""
lua << EOF
require("bufferline").setup{
  options = {
      numbers = "buffer_id",
      diagnostics = "coc",
      tab_size = 26,
      show_tab_indicators = true
    }
  }
EOF

lua << EOF
require("which-key").setup { }
EOF

lua << EOF
require("gitsigns").setup()
EOF

lua << EOF
require("todo-comments").setup {
  signs = true
}
EOF

lua << EOF
require("lualine").setup {
  options = {
    theme = "auto",
    component_separators = {left = '', right = ''},
    section_separators = {left = '', right = ''},
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch'},
    lualine_c = {
      'filename',
      'g:coc_status'
    },
    lualine_x = {{'filetype', colored = false}},
    lualine_y = {
      {
        'diagnostics',
        sources = {'coc'},
        diagnostics_color = {
          warn = '#fcba03',
          hint = '#5edb80',
          info = '#51e6fc',
        },
        symbols = {
          error = ' ',
          warn = ' ',
          info = ' ',
          hint = ' '
        }
      },
    },
    lualine_z = {{'location'}}
  }
}
EOF
