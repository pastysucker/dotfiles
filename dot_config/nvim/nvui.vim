NvuiScrollAnimationDuration 0.2
NvuiCursorAnimationDuration 0.1
NvuiMoveAnimationDuration 0.2

" Keybindings
map <S-Insert> <C-r>+
map! <S-Insert> <C-r>+

" Panes
nnoremap <A-CR> <cmd>vsplit<cr>
nnoremap <A-Up> <C-w><C-k>
nnoremap <A-Left> <C-w><C-h>
nnoremap <A-Down> <C-w><C-j>
nnoremap <A-Right> <C-w><C-l>
nnoremap <A-k> <C-w><C-k>
nnoremap <A-h> <C-w><C-h>
nnoremap <A-j> <C-w><C-j>
nnoremap <A-l> <C-w><C-l>

inoremap <A-CR> <esc><cmd>vsplit<cr>
inoremap <A-Up> <esc><C-w><C-k>
inoremap <A-Left> <esc><C-w><C-h>
inoremap <A-Down> <esc><C-w><C-j>
inoremap <A-Right> <esc><C-w><C-l>
inoremap <A-k> <esc><C-w><C-k>
inoremap <A-h> <esc><C-w><C-h>
inoremap <A-j> <esc><C-w><C-j>
inoremap <A-l> <esc><C-w><C-l>
