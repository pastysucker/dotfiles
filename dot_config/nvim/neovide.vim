" Options
let g:neovide_refresh_rate=60
let g:neovide_cursor_animation_length=0.05
let g:neovide_cursor_trail_length=0.01
let g:neovide_floating_blur=1
let g:neovide_floating_opacity=0.75

let g:neovide_cursor_vfx_mode = "pixiedust"
let g:neovide_cursor_vfx_opacity=200.0
let g:neovide_cursor_vfx_particle_lifetime=1.2
let g:neovide_cursor_vfx_particle_density=7.0
let g:neovide_cursor_vfx_particle_speed=10.0
let g:neovide_cursor_vfx_particle_phase=1.5
let g:neovide_cursor_vfx_particle_curl=1.0

" Keybindings
map <S-Insert> <C-r>+
map! <S-Insert> <C-r>+

" Panes
nnoremap <A-CR> <cmd>vsplit<cr>
nnoremap <A-Up> <C-w><C-k>
nnoremap <A-Left> <C-w><C-h>
nnoremap <A-Down> <C-w><C-j>
nnoremap <A-Right> <C-w><C-l>
nnoremap <A-k> <C-w><C-k>
nnoremap <A-h> <C-w><C-h>
nnoremap <A-j> <C-w><C-j>
nnoremap <A-l> <C-w><C-l>

inoremap <A-CR> <esc><cmd>vsplit<cr>
inoremap <A-Up> <esc><C-w><C-k>
inoremap <A-Left> <esc><C-w><C-h>
inoremap <A-Down> <esc><C-w><C-j>
inoremap <A-Right> <esc><C-w><C-l>
inoremap <A-k> <esc><C-w><C-k>
inoremap <A-h> <esc><C-w><C-h>
inoremap <A-j> <esc><C-w><C-j>
inoremap <A-l> <esc><C-w><C-l>

" work around temporary bug
set cmdheight=1

" proper ctrl-bs behaviour in cmdline
cmap <C-BS> <C-W>
