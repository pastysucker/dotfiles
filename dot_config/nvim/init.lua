-- START hibiscus.nvim bootstrap

local pack = "lazy"

local function bootstrap(url, ref)
    local name = url:gsub(".*/", "")
    local path

    if pack == "lazy" then
        path = vim.fn.stdpath("data") .. "/lazy/" .. name
        vim.opt.rtp:prepend(path)
    else
        path = vim.fn.stdpath("data") .. "/site/pack/".. pack .. "/start/" .. name
    end

    if vim.fn.isdirectory(path) == 0 then
        print(name .. ": installing in data dir...")

        vim.fn.system {"git", "clone", url, path}
        if ref then
            vim.fn.system {"git", "-C", path, "checkout", ref}
        end

        vim.cmd "redraw"
        print(name .. ": finished installing")
    end
end

-- for stable version [recommended]
bootstrap("https://github.com/udayvir-singh/tangerine.nvim")
bootstrap("https://github.com/udayvir-singh/hibiscus.nvim")

-- END BOOTSTRAP

local tangerine_dir = vim.fn.stdpath("data") .. "/tangerine"

require("tangerine").setup({
	target = tangerine_dir,
	keymaps = {
		eval_buffer = "<Nop>",
		peek_buffer = "<Nop>",
		goto_output = "<Nop>",
	},
	compiler = {
		verbose = false,
		float = false,
		hooks = { "onsave" },
	},
	eval = {
		float = true,
	},
})

local utils = require("utils")

vim.opt.updatetime = 700

-- truncate LSP log file to prevent it from getting too big
local log_files = {
	vim.lsp.get_log_path(),
	-- require("null-ls.logger"):get_path(), -- NOTE: bug in fn
}

for _, file in ipairs(log_files) do
	local handle = io.open(file, "w+")
	if handle ~= nil then
		handle:close()
	end
end

vim.fn.sign_define("DiagnosticSignError", { text = "" })
vim.fn.sign_define("DiagnosticSignWarn", { text = "" })
vim.fn.sign_define("DiagnosticSignHint", { text = "" })
vim.fn.sign_define("DiagnosticSignInfo", { text = "" })

-- disable some builtin vim plugins
local default_plugins = {
	"2html_plugin",
	"getscript",
	"getscriptPlugin",
	"gzip",
	"logipat",
	"netrw",
	"netrwPlugin",
	"netrwSettings",
	"netrwFileHandlers",
	"matchit",
	"tar",
	"tarPlugin",
	"rrhelper",
	"spellfile_plugin",
	"vimball",
	"vimballPlugin",
	"zip",
	"zipPlugin",
	"tutor",
	"rplugin",
	"syntax",
	"synmenu",
	"optwin",
	"compiler",
	"bugreport",
	"ftplugin",
}

utils["disable-builtin-plugins"](default_plugins)

local default_providers = {
	"node",
	"perl",
	"python3",
	"python",
	"ruby",
}

utils["disable-builtin-providers"](default_providers)

_G.dump = vim.pretty_print

require("core").setup()
require("plugins").setup()
require("lsp").setup()

if vim.fn.exists("g:neovide") == 1 then
	vim.cmd([[so ~/.config/nvim/neovide.vim]])
end
if vim.fn.exists("g:nvui") == 1 then
	vim.cmd([[so ~/.config/nvim/nvui.vim]])
end
