(require-macros :macros)

(macro use! [identifier ?options]
  "A workaround around the lack of mixed tables in Fennel.
  Has special `options` keys `config*` and `keys*`
  for enhanced utility.
  `identifier` should be a string.
  `?options` should be a table.
  Adapted from themis.nvim."
  (let [options (or ?options {})
        options (collect [k v (pairs options)]
                  (match k
                    :config* (values :config
                                     `#((. (require :plugins.configs) ,v)))
                    :keys* (let [entries (icollect [_ [lhs rhs ?opts] (ipairs v)]
                                           (let [opts (or ?opts {})
                                                 opts (collect [k1 v1 (pairs opts)]
                                                        (values k1 v1))]
                                             (doto opts
                                               (tset 1 lhs)
                                               (tset 2 rhs))))]
                             (values :keys entries))
                    _ (values k v)))]
    (doto options (tset 1 identifier))))

(local plugins [;;
                ;; better fennel experience
                (use! :udayvir-singh/tangerine.nvim)
                (use! :udayvir-singh/hibiscus.nvim)
                ;;
                ;; ----------
                ;; other deps
                ;; ----------
                (use! :romgrk/fzy-lua-native)
                ;;
                ;; --
                ;; ui
                ;; --
                ;; (use! :rcarriga/nvim-notify {:config* :notify})
                (use! :vigoux/notifier.nvim {:config* :notifier})
                (use! :stevearc/dressing.nvim)
                ;;
                ;; ------
                ;; syntax
                ;; ------
                (use! :sainnhe/sonokai {:config* :sonokai})
                ;;
                ;; ------
                ;; editor
                ;; ------
                ;; highlight color literals like red, #cba22f
                (use! :NvChad/nvim-colorizer.lua {:config* :colorizer})
                ;;
                ;; colorizer for LSP that support textDocument/documentColor
                (use! :mrshmllow/document-color.nvim
                      {:event [:BufRead]
                       :config #(module-setup :document-color {})})
                ;;
                ;; highlight rogue whitespace
                (use! :ntpeters/vim-better-whitespace
                      {:config* :vim-better-whitespace})
                ;;
                ;; start page
                (use! :goolord/alpha-nvim
                      {:dependencies [:kyazdani42/nvim-web-devicons]
                       :config* :alpha})
                ;;
                ;; markdown view
                (use! :toppair/peek.nvim
                      {:run "deno task --quiet build:fast" :config* :peek})
                ;;
                ;; side panel doc
                (use! :amrbashir/nvim-docs-view
                      {:lazy true :cmd [:DocsViewToggle] :config* :docs-view})
                ;;
                (use! :tpope/vim-repeat)
                ;;
                ;; range highlight
                (use! :winston0410/range-highlight.nvim
                      {:dependencies :winston0410/cmd-parser.nvim
                       :config* :range-highlight})
                ;;
                ;; type annotations
                (use! :danymat/neogen
                      {:cmd :Neogen
                       :dependencies :nvim-treesitter/nvim-treesitter
                       :config #(module-setup :neogen)})
                ;;
                ;; better folding experience
                (use! :kevinhwang91/nvim-ufo
                      {:dependencies :kevinhwang91/promise-async})
                ;;
                ;; prevent overuse of hjkl
                ;;(use! :ja-ford/delaytrain.nvim)
                ;;:config #(module-setup :delaytrain)
                ;;
                ;; ---------------------------------
                ;; file/project finding & navigation
                ;; ---------------------------------
                ;; tmux integration
                (use! :sunaku/tmux-navigate)
                ;;
                ;; terminal
                (use! :akinsho/toggleterm.nvim
                      {:branch :main :config* :toggleterm})
                ;;
                ;; multi purpose filter
                (use! :nvim-telescope/telescope.nvim
                      {:dependencies :nvim-lua/plenary.nvim
                       :event [:VimEnter]
                       :config #(module-setup :plugins.telescope)})
                (use! :nvim-telescope/telescope-fzy-native.nvim)
                (use! :FeiyouG/commander.nvim
                      {:dependencies :nvim-telescope/telescope.nvim
                       :config* :commander
                       :keys* [[:<leader>fc
                                "<CMD>Telescope commander<CR>"
                                {:mode :n}]]
                       :cmd "Telescope commander"})
                ;;
                ;; location list
                (use! :folke/trouble.nvim
                      {:dependencies :kyazdani42/nvim-web-devicons
                       :config* :trouble})
                ;;
                ;; surround word
                (use! :kylechui/nvim-surround {:config* :nvim-surround})
                ;;
                ;; tabout from surrounds
                (use! :abecodes/tabout.nvim
                      {:dependencies [:nvim-treesitter] :config* :tabout})
                ;;
                ;; autopairs
                (use! :windwp/nvim-autopairs {:config* :nvim-autopairs})
                ;;
                ;; paren mgmt for lisps
                (use! :eraserhd/parinfer-rust
                      {:build "cargo build --release"
                       :ft [:fennel :clojure :dune :query]})
                ;;
                ;; commenting
                (use! :numToStr/Comment.nvim {:config* :Comment})
                ;;
                ;; find & replace
                (use! :windwp/nvim-spectre
                      {:cmd [:Spectre]
                       :dependencies :nvim-lua/plenary.nvim
                       :config* :spectre})
                ;;
                ;; sneak plugin
                (use! :ggandor/leap.nvim {:config* :leap})
                (use! :ggandor/flit.nvim)
                (use! :ggandor/leap-spooky.nvim)
                ;;
                ;; popup keybindings
                (use! :folke/which-key.nvim
                      {:config* :which-key :event [:VimEnter]})
                ;;
                ;; indent guides
                (use! :lukas-reineke/indent-blankline.nvim
                      {:main :ibl :opts {} :config* :indent_blankline})
                ;;
                ;; auto indent function
                (use! :winston0410/smart-cursor.nvim)
                ;;
                ;; file managers
                (use! :mroavi/lf.vim {:config* :lf})
                ;;
                (use! :kyazdani42/nvim-tree.lua
                      {:cmd [:NvimTreeOpen :NvimTreeToggle]
                       :dependencies :kyazdani42/nvim-web-devicons
                       :keys* [[:<leader>e
                                :<cmd>NvimTreeToggle<cr>
                                {:desc :NvimTree}]]
                       :config* :nvim-tree})
                ;;
                ;; html tags
                (use! :windwp/nvim-ts-autotag
                      {:dependencies :nvim-treesitter/nvim-treesitter})
                ;;
                ;; note taking
                ; (use! :phaazon/mind.nvim
                ;       {:branch :v2.1
                ;        :dependencies :nvim-lua/plenary.nvim
                ;        :cmd [:MindOpenMain :MindOpenProject :MindReloadState]
                ;        :config* :mind})
                ;;
                ;; copilot chat
                (use! :CopilotC-Nvim/CopilotChat.nvim
                      {:branch :canary
                       :dependencies [[:zbirenbaum/copilot.lua]
                                      [:nvim-lua/plenary.nvim]]
                       :opts {:debug false :mappings {}}})
                ;;
                ;; buffer/mark/tab switcher
                ;;{ 1 "toppair/reach.nvim"}
                ;;
                ;; close buffers without messing up layout
                (use! :famiu/bufdelete.nvim)
                (use! :rcarriga/nvim-dap-ui
                      {:dependencies [:mfussenegger/nvim-dap
                                      :nvim-neotest/nvim-nio]
                       :config #(module-setup :dapui)})
                ;;
                ;; ------
                ;; STATUS
                ;; ------
                ;; scrollbar
                (use! :petertriho/nvim-scrollbar {:config* :scrollbar})
                ;;
                ;; git gutter
                (use! :lewis6991/gitsigns.nvim
                      {:event [:BufRead]
                       :dependencies :nvim-lua/plenary.nvim
                       :config* :gitsigns})
                ;;
                ;; git peek chunk
                (use! :tanvirtin/vgit.nvim
                      {:cmd :VGit
                       :lazy true
                       :dependencies [:nvim-lua/plenary.nvim]
                       :config #(module-setup :vgit)})
                ;;
                ;; todo comments
                (use! :folke/todo-comments.nvim
                      {:dependencies :nvim-lua/plenary.nvim
                       :event [:BufRead]
                       :config* :todo-comments})
                ;;
                ;; improved tabline
                (use! :akinsho/bufferline.nvim
                      {:branch :main :config* :bufferline})
                ;;
                ;; lsp-progress
                (use! :WhoIsSethDaniel/lualine-lsp-progress.nvim
                      {:dependencies [:nvim-lualine/lualine.nvim]})
                ;;
                ;; copilot
                (use! :AndreM222/copilot-lualine)
                ;;
                ;; statusline
                (use! :nvim-lualine/lualine.nvim
                      {:dependencies [:kyazdani42/nvim-web-devicons]
                       :config #(module-setup :plugins.lualine)})
                ;;
                ;; --------------
                ;; test and debug framework
                ;; --------------
                (use! :akinsho/neotest-go)
                (use! :sidlatau/neotest-dart)
                (use! :nvim-neotest/neotest
                      {:dependencies [:nvim-neotest/nvim-nio
                                      :nvim-lua/plenary.nvim
                                      :nvim-treesitter/nvim-treesitter]
                       :cmd :Neotest
                       :config* :neotest})
                (use! :mfussenegger/nvim-dap)
                ;;
                ;; ----------------------
                ;; language support & lsp
                ;; ----------------------
                :digitaltoad/vim-pug
                ;; pug
                :watzon/vim-edge-template
                ;; adonis edge syntax
                ;; purescript
                :purescript-contrib/purescript-vim
                :adelarsq/neofsharp.vim
                ;; json schemas
                :b0o/schemastore.nvim
                ;;
                ;; rainbow brackets
                :HiPhish/rainbow-delimiters.nvim
                (use! :JoosepAlviste/nvim-ts-context-commentstring
                      {:config #(module-setup :ts_context_commentstring
                                              {:enable_autocmd false})})
                :nvim-treesitter/nvim-treesitter-textobjects
                (use! :andymass/vim-matchup
                      {:event :VimEnter :config* :matchup})
                ;; extend % pair matchup
                (use! :nvim-treesitter/nvim-treesitter
                      {:build ":TSUpdate"
                       :dependencies [:rescript-lang/tree-sitter-rescript]
                       ;;:opts (fn [_ opts] (module-call :plugins.configs :treesitter _ opts))
                       :config #(module-setup :plugins.treesitter)})
                (use! :nvim-treesitter/playground)
                ;; navigation
                (use! :SmiteshP/nvim-navic
                      {:dependencies :neovim/nvim-lspconfig})
                ;;
                ;; LSP bundles
                ;; (1 "rescript-lang/vim-rescript"
                ;;       :event "BufRead"
                ;;       :ft "rescript")
                (use! :akinsho/flutter-tools.nvim
                      {:dependencies :nvim-lua/plenary.nvim})
                ;;(1 "jose-elias-alvarez/typescript.nvim"
                ;;      :dependencies "nvim-lua/plenary.nvim") ;; DISCONTINUED
                ; {1 :pmizio/typescript-tools.nvim
                ;  :dependencies [:nvim-lua/plenary.nvim :neovim/nvim-lspconfig]}
                (use! :mrcjkb/rustaceanvim {:version :^5 :lazy false})
                (use! :ray-x/go.nvim)
                (use! :p00f/clangd_extensions.nvim)
                ;;
                ;; snippets
                (use! :L3MON4D3/LuaSnip
                      {:event :BufRead
                       :version :v2.*
                       :config #(module-setup :luasnip.config {:history false})})
                :rafamadriz/friendly-snippets
                ;;
                ;; ai completions
                (use! :zbirenbaum/copilot.lua
                      {:cmd :Copilot
                       :event :InsertEnter
                       :config #(module-setup :copilot
                                              {:suggestion {:auto_trigger true
                                                            :hide_during_completion true
                                                            :debounce 100
                                                            :keymap {:accept false
                                                                     :accept_word :<C-S-Right>
                                                                     :accept_line :<C-Right>}}})})
                ; (use! :TabbyML/vim-tabby {:config #(tset vim.g :tabby_keybinding_accept "<Tab>")})
                ;;
                ;; native LSP
                (use! :junnplus/lsp-setup.nvim
                      {:dependencies [:neovim/nvim-lspconfig
                                      :williamboman/mason.nvim
                                      :williamboman/mason-lspconfig.nvim]})
                ;; additional functions like formatters and linters, successor to null-ls
                (use! :nvimtools/none-ls.nvim)
                ;;
                (use! :j-hui/fidget.nvim {:event :VimEnter :disable true})
                ;; lsp progress
                (use! :ray-x/lsp_signature.nvim {:event :VimEnter})
                (use! :hedyhli/outline.nvim
                      {:lazy true :event :VimEnter :config* :outline})
                ;; completions
                (use! :hrsh7th/nvim-cmp {:event [:VimEnter]})
                (use! :hrsh7th/cmp-nvim-lsp {:dependencies :nvim-cmp})
                (use! :hrsh7th/cmp-buffer {:dependencies :cmp-nvim-lsp})
                (use! :hrsh7th/cmp-path {:dependencies :cmp-buffer})
                (use! :hrsh7th/cmp-cmdline {:dependencies :cmp-path})
                (use! :saadparwaiz1/cmp_luasnip {:dependencies :cmp-cmdline})
                (use! :hrsh7th/cmp-nvim-lua {:dependencies :cmp_luasnip})
                (use! :tamago324/nlsp-settings.nvim)])

(fn setup []
  (let [lazypath (.. (vim.fn.stdpath :data) :/lazy/lazy.nvim)]
    (if (not (vim.loop.fs_stat lazypath))
        (vim.fn.system [:git
                        :clone
                        "--filter=blob:none"
                        "https://github.com/folke/lazy.nvim.git"
                        :--branch=stable
                        lazypath]))
    (vim.opt.rtp:prepend lazypath))
  (let [lazy (require :lazy)]
    (lazy.setup plugins {:profiling {:loader false :require false}})))

{: setup}
