(require-macros :macros)
(require-macros :hibiscus.vim)

(local utils (require :utils))

(var M {})

(fn M.notify []
  (module-setup :notify {:render :wrapped-compact})
  (set vim.notify (require :notify)))

(fn M.notifier []
  (module-setup :notifier {:component_name_recall true}))

(fn M.commander []
  (module-setup :commander
                {:components [:DESC :CAT]
                 :integration {:telescope {:enable true} :lazy {:enable false}}})
  (let [commands [{:desc "Flutter devices"
                   :cat :Flutter
                   :cmd :<CMD>FlutterDevices<CR>}
                  {:desc "Flutter restart"
                   :cat :Flutter
                   :cmd :<CMD>FlutterRestart<CR>}
                  {:desc "Setup Deno project"
                   :cat :Deno
                   :set false
                   :cmd #(utils.register-denols-commands)}
                  {:desc "Copilot panel"
                   :cat :Copilot
                   :cmd "<CMD>Copilot panel<CR>"}
                  {:desc "Copilot chat"
                   :cat :Copilot
                   :cmd :<CMD>CopilotChat<CR>}
                  {:desc "Generate commit message"
                   :cat :Copilot
                   :cmd :<CMD>CopilotChatCommit<CR>}
                  {:desc "Generate docs"
                   :cat :Copilot
                   :cmd :<CMD>CopilotChatDocs<CR>}
                  {:desc "Generate tests"
                   :cat :Copilot
                   :cmd :<CMD>CopilotChatTests<CR>}]
        commander (require :commander)]
    (commander.add commands)))

(fn M.sonokai []
  (g! sonokai_style :andromeda)
  (g! sonokai_enable_italic 1)
  (g! sonokai_disable_italic_comment 0)
  (g! sonokai_menu_selection_background :green)
  (g! sonokai_diagnostic_text_highlight 0)
  (g! sonokai_diagnostic_virtual_text :colored)
  (g! sonokai_better_performance 1)
  (color! :sonokai))

(fn M.colorizer []
  (let [colorizer (require :colorizer)]
    (colorizer.setup {:filetypes ["*"
                                  :!alpha
                                  :!packer
                                  :!css
                                  :!html
                                  :!tsx
                                  :!dart]
                      :buftypes ["*" :!alpha]
                      :user_default_options {:names false
                                             :css_fn true
                                             :RRGGBBAA true
                                             :AARRGGBB true}})))

(fn M.vim-better-whitespace []
  (augroup! :auto-strip-whitespace [[BufWritePre] * :StripWhitespace])
  (g! better_whitespace_filetypes_blacklist
      [:diff
       :git
       :gitcommit
       :help
       :markdown
       :fugitive
       :toggleterm
       :NeogitStatus]))

(fn M.alpha []
  (local alpha (require :alpha))
  (local startify (require :alpha.themes.startify))
  (local config startify.config)
  ;;(local Job (require :plenary.job))
  ;;(let [(fortune _) (: (Job:new {:command :fortune :args [:-s]}) :sync)
  ;;      (cowsay _) (: (Job:new {:command :cowsay
  ;;                             :args [(table.concat fortune "\n")]
  ;;                    :sync
  ;;  (set startify.section.header.val cowsay))
  (alpha.setup config))

(fn M.peek []
  (module-setup :peek {:auto_load false})
  (command! [] :PeekOpen #(module-call :peek :open))
  (command! [] :PeekClose #(module-call :peek :close)))

(fn M.docs-view []
  (module-setup :docs-view {:position :right :width 60}))

(fn M.range-highlight []
  (module-setup :range-highlight {}))

(fn M.toggleterm []
  (module-setup :toggleterm {:open_mapping "<C-`>"})
  (let [Terminal (module-get :toggleterm.terminal :Terminal)]
    ;; Lazygit
    (global Lazygit (Terminal:new {:cmd :lazygit
                                   :hidden true
                                   :direction :float}))
    (command! [] :LazygitToggle #(Lazygit:toggle))
    ;; Glow
    (global Glow (Terminal:new {:cmd "glow %"} :hidden true :direction
                               :vertical))
    (command! [] :Glow #(Glow:toggle))))

(fn M.trouble []
  (module-setup :trouble
                {:auto_close true
                 :focus true
                 :modes {:symbols {:mode :lsp_document_symbols
                                   :focus true
                                   :title false
                                   :win {:relative :win
                                         :position :right
                                         :size {:width 0.25}}}}}))

;; :use_diagnostic_signs true}))

(fn M.nvim-surround []
  (module-setup :nvim-surround {}))

(fn M.tabout []
  (module-setup :tabout {}))

(fn M.nvim-autopairs []
  (module-setup :nvim-autopairs
                {:disable_filetype [:fennel :TelescopePrompt] :check_ts true}))

(fn M.Comment []
  (module-setup :Comment
                {:pre_hook ((module-get :ts_context_commentstring.integrations.comment_nvim
                                        :create_pre_hook))})
  ;; mappings
  (map! [n] :<C-_> "<Plug>(comment_toggle_linewise_current)"
        "Comment whole line")
  (map! [n] :<C-/> "<Plug>(comment_toggle_linewise_current)"
        "Comment whole line")
  (map! [i] :<C-_> "<Esc><Plug>(comment_toggle_linewise_current)<esc>"
        "Comment line")
  (map! [i] :<C-/> "<Esc><Plug>(comment_toggle_linewise_current)<esc>"
        "Comment line")
  (map! [x] :<C-_> "<Plug>(comment_toggle_blockwise_visual)" "Comment block")
  (map! [x] :<C-/> "<Plug>(comment_toggle_blockwise_visual)" "Comment block"))

(fn M.spectre []
  (module-setup :spectre {})
  (command! [:nargs 0] :Spectre #(module-call :spectre :open)))

(fn M.leap []
  (module-call :leap :set_default_keymaps)
  (module-call :leap :init_highlight true)
  (let [hi #(vim.api.nvim_set_hl 0 :LeapBackdrop {:link :Comment})]
    (augroup! :LeapColors [[ColorScheme] * `hi])
    (hi))
  (module-setup :flit {})
  (module-setup :leap-spooky {}))

(fn M.which-key []
  (let [wk (require :which-key)]
    (wk.setup {:plugins {:spelling {:enabled true}}})
    (wk.add [{1 :<leader>f :group :Finding}
             {1 :<leader>fc
              2 "<CMD>Telescope commander<CR>"
              :desc "Command palette"}
             {1 :<leader>fs :group :Symbols}
             {1 :<leader>fss
              2 "<CMD>Telescope lsp_document_symbols<CR>"
              :desc "Find document symbols"}
             {1 :<leader>fsw
              2 "<CMD>Telescope lsp_dynamic_workspace_symbols<CR>"
              :desc "Find workspace symbols"}
             {1 :<leader>fp
              2 "<CMD>Telescope commands<CR>"
              :desc "Search all commands"}
             {1 :<leader>s :desc "Strip whitespace"}
             {1 :gL :desc "Show workspace diagnostics"}
             {1 :gb :desc "Pick a buffer to switch to"}
             {1 :gc :desc "Trigger commenting with motion"}
             {1 :gh :desc "Trigger hover preview from LSP"}
             {1 :gr :desc "Show all references of selected symbol"}
             {1 :gy :desc "Show type definition"}
             {1 :ys :desc "Trigger surrounding with motion"}])))

(fn M.indent_blankline []
  (module-setup :ibl {:exclude {:filetypes [:terminal
                                            :startify
                                            :packer
                                            :alpha
                                            :NvimTree
                                            :lspinfo
                                            :Trouble
                                            :lsp-installer
                                            :coctree]}}) ; :context_patterns [:declaration ;                    :expression ;                    :pattern ;                    :primary_expression ;                    :statement ;                    :switch_body ;                    :method ;                    :class ;                    :function ;                    :block ;                    :arguments]
  ;:show_current_context true
  ;:show_current_context_start false})
  (augroup! :indent-char
            [[ColorScheme]
             *
             #(do
                (vim.api.nvim_set_hl 0 :IndentBlanklineChar
                                     {:fg "#35474f" :nocombine true}))]))

;; (vim.api.nvim_set_hl 0 :IndentBlanklineContextStart
;;                     {:sp "#6dcae8" :underline true})
;; (vim.api.nvim_set_hl 0 :IndentBlanklineContextChar
;;                     {:fg "#6dcae8" :nocombine true}))]))

(fn M.lf []
  (g! "lf#set_default_mappings" 0)
  (g! "lf#layout" {:window {:width 0.9 :height 0.8 :highlight :Debug}})
  (map! [n] :<leader>fl :<cmd>LfPicker<CR> "Open Lf file manager"))

(fn M.nvim-tree []
  (module-setup :nvim-tree {:view {:signcolumn :no}
                            :hijack_cursor true
                            :disable_netrw true})
  (map! [n] :<leader>e :<cmd>NvimTreeToggle<cr> "Toggle file browser panel"))

(fn M.mind []
  (module-setup :mind))

(fn M.scrollbar []
  (module-setup :scrollbar))

(fn M.gitsigns []
  (module-setup :gitsigns))

(fn M.todo-comments []
  (module-setup :todo-comments
                {:signs true :highlight {:exclude [:md :markdown]}}))

(fn M.bufferline []
  (let [bufferline-opts {:options {:numbers :none
                                   :tab_size 16
                                   :max_name_length 24
                                   :enforce_regular_tabs false
                                   :show_tab_indicators true
                                   :show_buffer_close_icons false
                                   :show_close_icon false
                                   :always_show_bufferline false
                                   :offsets [{:filetype :NvimTree
                                              :text "File Explorer"
                                              :highlight :BufferLineBackground
                                              :text_align :center}]}}]
    (module-setup :bufferline bufferline-opts)
    (map! [n] :gB :<cmd>BufferLinePick<cr> "Pick a buffer to close")
    (map! [n] :dB :<cmd>BufferLinePickClose<cr> "Pick a buffer to delete")))

(fn M.matchup []
  (g! :matchup_matchparen_offscreen {:method :popup}))

(fn M.neotest []
  (module-setup :neotest
                {:adapters [((require :neotest-go) {})
                            ((require :neotest-dart) {})]}))

(fn M.treesitter [_ opts]
  (let [parser-config (module-call :nvim-treesitter.parsers :get_parser_configs)]
    (tset parser-config :rescript :install_info
          {:url "https://github.com/rescript-lang/tree-sitter-rescript"
           :branch :main
           :files [:src/scanner.c]
           :generate_requires_npm false
           :requires_generate_from_grammar true})
    (tset parser-config :fsharp
          {:install_info {:url "https://github.com/ionide/tree-sitter-fsharp"
                          :branch :main
                          :files [:src/scanner.c :src/parser.c]}
           :requires_generate_from_grammar false
           :filetype :fsharp})))

(fn M.outline []
  (module-setup :outline
                {:preview_window {:auto_preview true}
                 :symbols {:icons {:File {:icon " "}
                                   :Module {:icon " "}
                                   :Namespace {:icon " "}
                                   :Package {:icon " "}
                                   :Class {:icon " "}
                                   :Method {:icon " "}
                                   :Property {:icon " "}
                                   :Field {:icon " "}
                                   :Constructor {:icon " "}
                                   :Enum {:icon " "}
                                   :Interface {:icon " "}
                                   :Function {:icon " "}
                                   :Variable {:icon " "}
                                   :Constant {:icon " "}
                                   :String {:icon " "}
                                   :Number {:icon " "}
                                   :Boolean {:icon " "}
                                   :Array {:icon " "}
                                   :Object {:icon " "}
                                   :Key {:icon " "}
                                   :Null {:icon " "}
                                   :EnumMember {:icon " "}
                                   :Struct {:icon " "}
                                   :Event {:icon " "}
                                   :Operator {:icon " "}
                                   :TypeParameter {:icon " "}}}}))

M
