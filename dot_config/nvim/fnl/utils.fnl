(fn get-sign [name]
  (. (. (vim.fn.sign_getdefined name) 1) :text))

(fn get-hl-color [group]
  (vim.api.nvim_get_hl_by_id (vim.fn.hlID group) true))

(fn set-highlights [tbl]
  (each [k v (pairs tbl)]
    (vim.api.nvim_set_hl 0 k v)))

(fn disable-builtin-plugins [plugins]
  (each [_ plugin (pairs plugins)]
    (var loaded-plugin (. vim.g (.. :loaded_ plugin)))
    (set loaded-plugin 1)))

(fn disable-builtin-providers [providers]
  (each [_ provider (pairs providers)]
    (var loaded-provider (. vim.g (.. :loaded_ provider :_provider)))
    (set loaded-provider 1)))

(fn init-mapping [mode]
  (fn [lhs rhs opts]
    (vim.keymap.set mode lhs rhs opts)))

(fn init-mapping-with-prefix [prefix mode]
  (fn [lhs rhs opts]
    (vim.keymap.set mode (.. prefix lhs) rhs opts)))

(fn init-bindings []
  (local map (init-mapping [:n :v :o]))
  (local nmap (init-mapping :n))
  (local imap (init-mapping :i))
  (local xmap (init-mapping :x))
  {: map : nmap : imap : xmap})

{: get-sign
 : get-hl-color
 : set-highlights
 : disable-builtin-plugins
 : disable-builtin-providers
 : init-mapping
 : init-mapping-with-prefix
 : init-bindings}
