(import-macros {: command!} :hibiscus.vim)

(fn setup-sources []
  (local builtins (require :null-ls.builtins))
  (local helpers (require :null-ls.helpers))
  [(builtins.formatting.prettierd.with {:prefer_local :node_modules/.bin
                                        :filetypes [:javascript
                                                    :javascriptreact
                                                    :typescript
                                                    :typescriptreact]
                                        :condition #(not ($.root_has_file [:dprint.json
                                                                           :.dprint.json
                                                                           :deno.json
                                                                           :deno.jsonc]))})
   builtins.formatting.black
   builtins.formatting.fnlfmt
   builtins.formatting.shfmt
   builtins.formatting.stylua
   builtins.diagnostics.stylelint])

(fn setup []
  (local null-ls (require :null-ls))
  (local handlers (require :lsp.handlers))
  (null-ls.setup {:on_attach (handlers.on-attach) :sources (setup-sources)})
  (command! [:nargs 1] :NullLsDisable
            #(do
               (null-ls.disable $.args)
               (print (.. "Trying to disable " $.args))))
  (command! [:nargs 1] :NullLsEnable
            #(do
               (null-ls.enable $.args)
               (print (.. "Trying to enable " $.args)))))

{: setup}
