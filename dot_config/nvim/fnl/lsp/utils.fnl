(import-macros {: command!} :hibiscus.vim)
(import-macros {: nil? } :hibiscus.core)

(macro echoerr! [arg]
  `(vim.api.nvim_err_writeln ,arg))

(macro notify! [arg]
  `(vim.notify ,arg vim.log.levels.INFO {}))

(fn setup-deno-project [client]
  (local conf-json "{\"deno.enable\": true,
\"deno.lint\": true,
\"deno.importMap\": \"./import_map.json\",
\"deno.config\": \"./deno.json\"}")
  (local nt-utils (require :nvim-tree.utils))
  (local root-dir client.config.root_dir)
  (local config-file (.. root-dir :/.nlsp-settings/denols.json))
  (if (nt-utils.file_exists config-file)
      (print (.. config-file " already exists"))
      (let [dir-ok (vim.loop.fs_mkdir :.nlsp-settings (tonumber :0777 8))]
        (if (not dir-ok)
            (echoerr! ".nlsp-settings already exists")
            (match (pcall vim.loop.fs_open config-file :w (tonumber :0644 8))
              (false nil) (vim.api.nvim_err_writeln "Couldn't create LSP config file")
              (_ fd) (do
                       (vim.loop.fs_write fd conf-json)
                       (vim.loop.fs_close fd)
                       (notify! "denols.json file created!")))))))

(lambda register-denols-commands [client]
  (command! [] :DenolsInit #(setup-deno-project client)))

(lambda with-exclusive-format-keymap [table]
  (fn [client bufnr]
    (let [keymap {:<leader> {:F [(fn []
                                   (let [util (require :vim.lsp.util)
                                         params (util.make_formatting_params {})]
                                     (client.request :textDocument/formatting
                                                     params nil bufnr)))
                                 "Format current file"]}}]
      (if (nil? table)
          keymap
          (vim.tbl_deep_extend :force keymap table)))))

{: register-denols-commands : with-exclusive-format-keymap}
