(require-macros :hibiscus.vim)
(import-macros {: module-setup : module-get : module-call} :macros)

(local lspconfig-util (require :lspconfig.util))

(local handlers (require :lsp.handlers))
(local capabilities (handlers.make-capabilities))

(fn with-handlers [opts]
  (let [handler-opts {:on_attach (handlers.on-attach) : capabilities}]
    (if opts
        (vim.tbl_deep_extend :force handler-opts opts)
        handler-opts)))

(local tsserver-root-dir (lspconfig-util.root_pattern :package.json))
(local denols-root-dir (lspconfig-util.root_pattern :deno.json :deno.jsonc))

(fn servers []
  {:clangd {}
   :clojure_lsp {}
   :cssls {}
   :dartls (module-setup :flutter-tools
                         {:lsp (with-handlers {})
                          :dev_log {:enabled false
                                    :notify_errors false
                                    :open_cmd "botright 7 split"}
                          :widget_guides {:enabled true}
                          :decorations {:statusline {:device true}}
                          :debugger {:enabled true :run_via_dap true}})
   :denols {:on_attach (handlers.on-attach {:exclusive-formatting true})
            :root_dir #(and (not (tsserver-root-dir $)) (denols-root-dir $))}
   :emmet_ls {;:cmd [:/home/tamim/Programming/emmet-ls/out/server.js :--stdio]
              :filetypes [:html
                          :css
                          :sass
                          :scss
                          :less
                          :edge
                          :javascriptreact
                          :typescriptreact]}
   :fsautocomplete {:single_file_support true}
   :html {}
   :jsonls {:filetypes [:json :jsonc :json5]
            :on_attach (handlers.on-attach {:exclusive-formatting true})
            :settings {:json {:schemas ((. (module-get :schemastore :json)
                                           :schemas))}
                       :validate {:enable true}}}
   :kotlin_language_server {}
   :lua_ls {:on_attach (handlers.on-attach {:disable-formatter true})}
   :phpactor {}
   :purescriptls {}
   :pyright {}
   :rescriptls {:cmd [:/home/tamim/.local/share/nvim/mason/bin/rescript-language-server
                      :--stdio]}
   ; :rust_analyzer (module-setup :rust-tools
   ;                              {:server {:on_attach (handlers.on-attach)
   ;                                        :settings {:rust-analyzer {:cargo {:loadOutDirsFromCheck true}
   ;                                                                   :procMacro {:enable true}}}}})
   :svelte {}
   :tsserver {:init_options {:plugins [{:name "@vue/typescript-plugin"
                                        :location "/home/tamim/.volta/tools/shared/@vue/typescript-plugin"
                                        :languages [:javascript
                                                    :typescript
                                                    :vue]}]}
              :filetypes [:javascript :typescript :vue]
              :single_file_support false
              :root_dir #(and (not (denols-root-dir $)) (tsserver-root-dir $))}
   :vimls {}
   :volar {}
   :zls {}})

(fn setup-servers []
  (local gopls-path (.. (module-call :mason-core.path :bin_prefix) :/gopls))
  "
  (tset (require :lspconfig.configs) :fennel_language_server
        {:default_config {:cmd [:fennel-language-server]
                          :filetypes [:fennel]
                          :single_file_support true
                          :root_dir (lspconfig-util.root_pattern :fnl :main.fnl
                                                                 :init.fnl)
                          :settings {:fennel {:workspace {:library (vim.api.nvim_list_runtime_paths)}
                                              :diagnostics {:globals [:vim
                                                                      :love]}}}}})
  "
  (augroup! :lspconfig [[LspAttach]
                        *.rs
                        #(let [bufnr $1.buf
                               client (vim.lsp.get_client_by_id $1.data.client_id)
                               on-attach (handlers.on-attach)]
                           (on-attach client bufnr))])
  (module-setup :lsp-setup {:default_mappings false
                            :on_attach (handlers.on-attach)
                            : capabilities
                            :servers (servers)})
  (module-setup :go {:gopls_cmd [gopls-path]
                     :lsp_cfg (with-handlers {})
                     :lsp_gofumpt true
                     :trouble true
                     :luasnip true}))

(fn setup []
  (module-setup :nlspsettings {:config_home (.. (vim.fn.stdpath :config)
                                                :/fnl/lsp/nlsp-settings)
                               :local_settings_dir :.nlsp-settings
                               :local_settings_root_markers [:.git]
                               :append_default_schemas true
                               :loader :json})
  (setup-servers))

{: setup}
