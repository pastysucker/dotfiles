(require-macros :macros)

(fn setup []
  (module-setup :lsp.lsp-installer)
  (module-setup :lsp.handlers)
  (module-setup :lsp.null-ls)
  (module-setup :lsp.cmp))

{: setup}
