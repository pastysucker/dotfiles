(require-macros :hibiscus.vim)
(require-macros :hibiscus.core)
(require-macros :macros)

(macro nmap! [lhs rhs desc bufnr]
  `(vim.keymap.set :n ,lhs ,rhs {:silent true :desc ,desc :buffer ,bufnr}))

(fn _G.LspDiagnosticPopupHandler []
  "Show diagnostics in a popup window on hover"
  (local current-cursor (vim.api.nvim_win_get_cursor 0))
  (local last-popup-cursor (or vim.w.lsp_diagnostics_last_cursor [nil nil]))
  "Show popup diagnostic window"
  "but only once for current cursor location"
  (let [current-cursor-x (. current-cursor 1)
        current-cursor-y (. current-cursor 2)
        last-popup-cursor-x (. last-popup-cursor 1)
        last-popup-cursor-y (. last-popup-cursor 2)]
    (when (and (not= current-cursor-x last-popup-cursor-x)
               (not= current-cursor-y last-popup-cursor-y))
      (do
        (set vim.w.lsp_diagnostics_last_cursor current-cursor)
        (vim.diagnostic.open_float 0 {:scope :cursor :focus false})))))

(lambda diagnostic-format [diagnostic]
  (let [user-data (or diagnostic.user_data diagnostic.user_data.lsp
                      diagnostic.user_data.null_ls {})
        code (or diagnostic.symbol diagnostic.code user-data.symbol
                 user-data.code)]
    (if (not= code nil)
        (string.format "%s (%s)" diagnostic.message code)
        diagnostic.message)))

(local float-opts {:focusable false
                   :style :minimal
                   :border [" " "" " " " " " " "" " " " "]
                   :source :always
                   :header ""
                   :prefix ""
                   :max_width 70
                   :format diagnostic-format})

(fn hover [_ result ctx config]
  (when (not= result nil)
    (let [markdown-lines (doto (vim.lsp.util.convert_input_to_markdown_lines result.contents)
                           (vim.lsp.util.trim_empty_lines))
          config (or config {})]
      (set config.focus_id ctx.method)
      (when (not (empty? markdown-lines))
        (vim.lsp.util.open_floating_preview markdown-lines :markdown config)))))

(fn setup []
  ;; lsp progress spinner
  ;; (module-setup :fidget {:text {:spinner :dots} :window {:blend 0}})
  ;; setup modern fold provider
  (module-setup :ufo)
  ;; diagnostic on cursorhold
  (augroup! :reset-group [[:CursorHold] * #(_G.LspDiagnosticPopupHandler)])
  ;; set signs
  (let [signs [{:name :DiagnosticSignError :text ""}
               {:name :DiagnosticSignWarn :text ""}
               {:name :DiagnosticSignInfo :text ""}
               {:name :DiagnosticSignHint :text ""}]
        diagnostic-config {:virtual_text {:severity {:max vim.diagnostic.severity.ERROR
                                                     :min vim.diagnostic.severity.WARN}
                                          :prefix ""}
                           :signs {:active signs}
                           :update_in_insert false
                           :underline true
                           :severity_sort true
                           :float float-opts}]
    (each [_ sign (ipairs signs)]
      (vim.fn.sign_define sign.name
                          {:texthl sign.name :text sign.text :numhl ""}))
    (tset vim.lsp.handlers :textDocument/hover (vim.lsp.with hover float-opts))
    (tset vim.lsp.handlers :textDocument/signatureHelp
          (vim.lsp.with vim.lsp.handlers.signature_help float-opts))
    (tset vim.lsp.handlers :textDocument/publishDiagnostics
          (vim.lsp.with vim.lsp.diagnostic.on_publish_diagnostics
                        diagnostic-config))))

(fn lsp-highlight-document [client bufnr]
  (when (and client.server_capabilities.documentHighlightProvider
             (not= client.name :fsautocomplete))
    ;; fsautocomplete throws constant errors
    (let [lsp-document-highlight (vim.api.nvim_create_augroup :lsp_document_highlight
                                                              {:clear true})]
      (augroup! :lsp_document_highlight
                [[CursorHold]
                 `(buffer bufnr)
                 #(vim.lsp.buf.document_highlight)]
                [[CursorMoved] `(buffer bufnr) #(vim.lsp.buf.clear_references)]))))

(fn lsp-register-keymaps [client bufnr opts]
  (local extra-keymaps (or opts.keymaps {}))
  (local exclusive-formatting (or opts.exclusive-formatting false))
  (nmap! :gD #(vim.lsp.buf.declaration) "Go to declaration" bufnr)
  (nmap! :<F12> #(module-call :trouble :open {:mode :lsp_definitions})
         "Jump to definition" bufnr)
  (nmap! :gh #(vim.lsp.buf.hover) "Show LSP hover information" bufnr)
  (nmap! :gi #(module-call :trouble :open {:mode :lsp_implementations})
         "See implementations of this symbol" bufnr)
  (nmap! :<F2> #(vim.lsp.buf.rename) "Rename symbol" bufnr)
  (nmap! :gr #(module-call :trouble :open {:mode :lsp_references})
         "See references" bufnr)
  (nmap! :<C-.> #(vim.lsp.buf.code_action) "Display code actions" bufnr)
  (nmap! "[d" #(vim.diagnostic.goto_prev) "Jump to previous diagnostic" bufnr)
  (nmap! "]d" #(vim.diagnostic.goto_next) "Jump to next diagnostic" bufnr)
  (nmap! :gl #(vim.lsp.diagnostic.open_float) "Show diagnostic under cursor"
         bufnr)
  (nmap! :gL #(module-call :trouble :open {:mode :diagnostics})
         "Show diagnostic under cursor" bufnr)
  (nmap! :<leader>F
         #(if exclusive-formatting
              (let [params (vim.lsp.util.make_formatting_params {})]
                (client.request :textDocument/formatting params nil bufnr))
              (vim.lsp.buf.format {:async true})) "Format current file"
         bufnr))

(fn on-attach [arg]
  "AttachArgs {:keymaps Table :disable-formatter Bool :exclusive-formatting Bool}"
  (lambda [client bufnr]
    (local active-clients
           (vim.tbl_map #(. $ :name) (vim.lsp.buf_get_clients bufnr)))
    (local length-clients (length active-clients))
    (when (and (= client.name :null-ls) (> length-clients 1))
      "Don't re-run procedure if handlers already set up"
      (lua return))
    ;;;;
    (local default-opts {:keymaps {}
                         :exclusive-formatting false
                         :disable-formatter false})
    (local opts (if (fn? arg)
                    (vim.tbl_deep_extend :keep (arg client bufnr) default-opts)
                    (vim.tbl_deep_extend :keep (or arg default-opts)
                                         default-opts)))
    (when opts.disable-formatter
      (set client.server_capabilities.documentFormattingProvider false))
    (module-call :lsp_signature :on_attach
                 {:handler_opts float-opts
                  :doc_lines 0
                  :floating_window true
                  :hint_enable false
                  :auto_close_after 5
                  :toggle_key :<C-.>} bufnr)
    (lsp-register-keymaps client bufnr opts)
    (lsp-highlight-document client bufnr)
    ;;
    (when client.server_capabilities.colorProvider
      (module-call :document-color :buf_attach bufnr))
    (when client.server_capabilities.documentSymbolProvider
      (module-call :nvim-navic :attach client bufnr))))

(fn make-capabilities []
  (local capabilities {:textDocument {}})
  (set capabilities.textDocument.foldingRange
       {:dynamicRegistration false :lineFoldingOnly true})
  (set capabilities.textDocument.colorProvider {:dynamicRegistration true})
  (local cmp-capabilities (module-call :cmp_nvim_lsp :default_capabilities))
  (vim.tbl_deep_extend :force capabilities cmp-capabilities))

{: setup : on-attach : make-capabilities}
