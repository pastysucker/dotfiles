(fn setup []
  (let [editor (require :core.editor)]
    (editor.setup))
  (require :core.keymaps))

{: setup}
