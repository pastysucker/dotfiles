(require-macros :hibiscus.vim)
(local utils (require :utils))

(fn setup []
  (when (= (vim.fn.exists :+termguicolors) 1)
    (set! termguicolors))
  ;; -- ;;
  ;; UI ;;
  ;; -- ;;
  (g! loaded_netrwFileHandlers 1)
  (g! loaded_netrw_gitignore 1)
  (g! loaded_netrwSettings 1)
  (g! loaded_netrwPlugin 1)
  (g! loaded_netrw 1)
  (when (= (vim.fn.has :nvim) 1)
    (vim.fn.setenv :NVIM_TUI_ENABLE_TRUE_COLOR 1))
  (set! nowildmenu)
  (local highlights {:DiagnosticError {:ctermfg 1 :fg "#d1666a"}
                     :DiagnosticWarn {:ctermfg 3 :fg "#d1cd66"}
                     :DiagnosticInfo {:ctermfg 12 :fg "#307fd9"}
                     :DiagnosticHint {:ctermfg 2 :fg "#30d974"}
                     :DiagnosticUnderlineHint {:underline true
                                               :underdashed true
                                               :sp :Grey}})
  (augroup! :colorscheme-loaded
            [[ColorScheme] * #(utils.set-highlights highlights)])
  ;; ------ ;;
  ;; EDITOR ;;
  ;; ------ ;;
  (augroup! :auto-line-numbers [[InsertEnter] * #(set! norelativenumber)]
            [[InsertLeave] * #(set! relativenumber)])
  (set! re 0) ; use new highlighting engine
  (set! encoding :utf-8)
  (set! guifont "JetBrainsMono Nerd Font:h11")
  (set! cmdheight 1)
  (set! mouse :nvi) ; use mouse for everything
  (set! noshowmode) ; don't show current mode in cmdline
  (set! laststatus 3) ; always display statusline
  (set! cursorline) ; highlight current line
  (set! ruler)
  (set! number) ; show line numbers
  (set! backspace "indent,eol,start") ; backspace everywhere
  (set! autoindent) ; auto indent for newline
  (set! smartindent)
  (set! tabstop 2)
  (set! shiftwidth 2)
  (set! expandtab)
  (set! linebreak) ; break lines on words instead of in middle
  (set! breakindent) ; wrapping lines is indented
  (set! hlsearch) ; highlight search terms
  (set! list) ; show whitespace
  (set! autoread) ; auto reload files on change
  (set! scrolloff 5) ; minimum padding around cursorline
  (set! virtualedit :onemore) ; virtual cursor word
  (set! backupdir (vim.fn.expand "~/.local/share/nvim/_backup"))
  (set! directory (vim.fn.expand "~/.local/share/nvim/_temp"))
  (set! spellfile (vim.fn.expand "~/.local/share/nvim/spell/en_us.utf-8.add")) ; persistent undo history
  ;; persistent undo history
  (when (= (vim.fn.has :persistent_undo) 1)
    (set! undodir (vim.fn.expand "~/.local/share/nvim/_undo"))
    (set! undofile true))
  ;; goto last editing position
  (augroup! :editing-position
            [[BufReadPost]
             *
             (fn []
               (local (row column) (unpack (vim.api.nvim_buf_get_mark 0 "\"")))
               (local buf-line-count (vim.api.nvim_buf_line_count 0))
               (when (and (>= row 1) (<= row buf-line-count))
                 (vim.api.nvim_win_set_cursor 0 [row column])))])
  ;; highlight on yank
  (augroup! :highlight-yank
            [[TextYankPost :desc "highlights yanked region"]
             *
             #(vim.highlight.on_yank {:higroup :DiffChange :timeout 200})])
  ;; ------------- ;;
  ;; PANES/BUFFERS ;;
  ;; ------------- ;;
  (set! splitright)
  (set! noequalalways) ; prevents splits from auto adjusting horizontally on close
  (set! foldmethod :indent) ; fold based on indentation
  (set! foldlevel 99)
  (set! nofoldenable) ; don't open a file folded by default
  (set! signcolumn :yes) ; always show signcolumn
  (set! nowrap)
  (set! title) ; set window title to filename
  (set! titlestring "%f%( [%M]%)"))

{: setup}
