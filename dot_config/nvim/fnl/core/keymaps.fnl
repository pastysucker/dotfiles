(require-macros :hibiscus.vim)

(g! mapleader " ")
(map! [n :verbose] ";" ":")

;; --------------- ;;
;; File management ;;
;; --------------- ;;
(map! [nvo] :<C-s> ":w<CR>")
(map! [i :verbose] :<C-s> "<ESC>:w<CR>")
(map! [n] :<leader>q ":q<CR>" :Quit)
(map! [n :verbose] :<leader>y "\"+y" "Copy into system clipboard")
(map! [n :verbose] :<leader>p "\"+p" "Paste from system clipboard")
(map! [ict] :<C-BS> :<C-W>)
(map! [n] :<CR> ":noh<CR><CR>")

;; ------- ;;
;; Buffers ;;
;; ------- ;;
(map! [n] :<C-Tab> ":bnext<CR>")
(map! [n] :<C-S-Tab> ":bprevious<CR>")

(map! [n] :<C-h> ":bprevious<CR>")
(map! [n] :<C-l> ":bnext<CR>")
(map! [i] :<C-h> "<Esc>:bprevious<CR>")
(map! [i] :<C-l> "<Esc>:bnext<CR>")

(map! [n] :QQ ":Bdelete<CR>")
(map! [n] :Qq ":%bdelete<CR>")

;; --------------- ;;
;; Panels & splits ;;
;; --------------- ;;
(map! [n] :<C-y> ":terminal<CR>")

(map! [n] :<S-t> ":resize -10<CR>")
(map! [n] :<S-c> ":resize +10<CR>")

;; ------- ;;
;; Editing ;;
;; ------- ;;
(map! [i] :jk :<ESC>)

;; copying/pasting
(map! [n] "\"+p"
      ":let @\"=substitute(system(\"wl-paste --no-newline\"), '<C-v><C-m>', '', 'g')<cr>p"
      "Paste from system clipboard")

(map! [n] "\"*p"
      ":let @\"=substitute(system(\"wl-paste --no-newline --primary\"), '<C-v><C-m>', '', 'g')<cr>p"
      "Paste from primary system clipboard")

(map! [x] "\"+y" "y:call system(\"wl-copy\", @\")<cr>"
      "Copy to system clipboard")

;; recursive unfold
(map! [n] :<leader>z :zczA "Recursive unfold")

;; insert blank lines around cursor
(map! [n] "]<Space>"
      "o<CR><Up><Esc>:lua require(\"smart-cursor\").indent_cursor()<cr>a"
      "Surround line with padding")

;; indent cursor
(map! [n] :<leader><Tab> ":lua require(\"smart-cursor\").indent_cursor()<cr>a"
      "Smart indent cursor")

(augroup! :easy-quit-on-filetype
          [[FileType]
           [:help :mind :spectre_panel]
           #(map! [n :buffer] :q ":q<CR>")])
