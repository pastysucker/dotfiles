(lambda module-call [origin lookup ...]
  (let [output [...]]
    `(do
       ((. (require ,origin) ,lookup) ,...))))

(lambda module-get [origin ...]
  (let [output [...]]
    `(. (require ,origin) ,...)))

(lambda module-setup [origin ...]
  (let [output [...]]
    `((. (require ,origin) :setup) ,...)))

{: module-call : module-setup : module-get}
