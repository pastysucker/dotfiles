#!/usr/bin/env zsh
# zsh completion support for vr v1.4.0

autoload -U is-at-least

# shellcheck disable=SC2154
(( $+functions[__vr_complete] )) ||
function __vr_complete {
  local name="$1"; shift
  local action="$1"; shift
  integer ret=1
  local -a values
  local expl lines
  _tags "$name"
  while _tags; do
    if _requested "$name"; then
      # shellcheck disable=SC2034
      lines="$(vr completions complete "${action}" "${@}")"
      values=("${(ps:\n:)lines}")
      if (( ${#values[@]} )); then
        while _next_label "$name" expl "$action"; do
          compadd -S '' "${expl[@]}" "${values[@]}"
        done
      fi
    fi
  done
}

# shellcheck disable=SC2154
(( $+functions[_vr] )) ||
function _vr() {
  local state

  function _commands() {
    local -a commands
    # shellcheck disable=SC2034
    commands=(
      'run:Run a script'
      'export:Export one or more scripts as standalone executable files'
      'upgrade:Upgrade Velociraptor to the latest version or to a specific one'
    )
    _describe 'command' commands
    __vr_complete script scriptid 
  }

  function _command_args() {
    case "${words[1]}" in
      run) _vr_run ;;
      export) _vr_export ;;
      upgrade) _vr_upgrade ;;
    esac
  }

  _arguments -w -s -S -C \
    '(- *)'{-h,--help}'[Show this help.]' \
    '(- *)'{-V,--version}'[Show the version number for this program.]' \
    '1: :_commands'\
    '2::additionalArgs-string' \
    '*:: :->command_args'

  case "$state" in
    command_args) _command_args ;;
    additionalArgs-string) __vr_complete additionalArgs string  ;;
  esac
}

# shellcheck disable=SC2154
(( $+functions[_vr_run] )) ||
function _vr_run() {

  function _commands() {
    __vr_complete script scriptid run
  }

  _arguments -w -s -S -C \
    '(- *)'{-h,--help}'[Show this help.]' \
    '1: :_commands'\
    '2::additionalArgs-string'

  case "$state" in
    additionalArgs-string) __vr_complete additionalArgs string run ;;
  esac
}

# shellcheck disable=SC2154
(( $+functions[_vr_export] )) ||
function _vr_export() {

  function _commands() {
    __vr_complete scripts scriptid export
  }

  _arguments -w -s -S -C \
    '(- *)'{-h,--help}'[Show this help.]' \
    '(-h --help -o --out-dir)'{-o,--out-dir}'[The folder where the scripts will be exported]::dir:->dir-string' \
    '1: :_commands'

  case "$state" in
    dir-string) __vr_complete dir string export ;;
  esac
}

# shellcheck disable=SC2154
(( $+functions[_vr_upgrade] )) ||
function _vr_upgrade() {

  _arguments -w -s -S -C \
    '(- *)'{-h,--help}'[Show this help.]'
}

# _vr "${@}"

compdef _vr vr

