export PATH=$HOME/.poetry/bin:$HOME/.appimages:$HOME/.pub-cache/bin:$HOME/go/bin:$HOME/.volta/bin:$HOME/.cargo/bin:$HOME/.deno/bin:$HOME/flutter/bin:$HOME/.dotnet/tools:$HOME/.luarocks/bin:$HOME/.nimble/bin:$HOME/.bun/bin:$HOME/.local/share/neovim/bin:$HOME/.local/share/nvim/mason/bin:$PATH

fpath+=./.zfunc

export EDITOR=nvim
# export TERM=tmux
export CHROME_EXECUTABLE=chromium

# -- Volta
export VOLTA_HOME=$HOME/.volta

# -- ZSH Auto Notify
export AUTO_NOTIFY_THRESHOLD=20
export AUTO_NOTIFY_TITLE="%command has just finished"
export AUTO_NOTIFY_BODY=""
export AUTO_NOTIFY_EXPIRE_TIME=4500
AUTO_NOTIFY_IGNORE+=("lf" "feh" "kak" "bat" "lazygit")

# -- ZSH Notify
zstyle ':notify:*' error-title "Command failed (in #{time_elapsed} seconds)"
zstyle ':notify:*' success-title "Command finished (in #{time_elapsed} seconds)"
zstyle ':notify:*' disable-urgent yes
zstyle ':notify:*' command-complete-timeout 20 # seconds
zstyle ':notify:*' expire-time 2500 # milliseconds
zstyle ':notify:*' always-notify-on-failure no
zstyle ':notify:*' blacklist-regex 'find|lf|feh|kak|bat|vim|nvim|imv'

# -- Zsh Autosuggest
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#666"
ZSH_AUTOSUGGEST_STRATEGY=(history completion match_prev_cmd)
# ZSH_AUTOSUGGEST_COMPLETION_IGNORE="*pacman*"
ZSH_AUTOSUGGEST_USE_ASYNC=1

# -- Zsh history substring search
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND="bg=23,fg=15"
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND="bg=88,fg=15"

# -- Enhancd
export ENHANCD_FILTER="fzf --height=5% --layout=reverse"

# -- FZF
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --exclude .venv --exclude .cache --exclude node_modules'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'pistol {}'"
export FZF_ALT_C_OPTS="--preview 'exa --tree {} | head -200'"

# -- Brew
# eval $(/home/tamim/.linuxbrew/bin/brew shellenv)

# -- Neovide
export NEOVIDE_MULTIGRID=1

# -- pkg-config
export PKG_CONFIG=/usr/bin/pkg-config

# -- Bun
export BUN_INSTALL="$HOME/.bun"

# -- lf
export LF_ICONS="\
tw=:\
st=:\
ow=:\
dt=:\
di=:\
fi=:\
ln=:\
or=:\
ex=:\
*.c=:\
*.cc=:\
*.clj=:\
*.coffee=:\
*.cpp=:\
*.css=:\
*.d=:\
*.dart=:\
*.erl=:\
*.exs=:\
*.fs=:\
*.go=:\
*.h=:\
*.hh=:\
*.hpp=:\
*.hs=:\
*.html=:\
*.java=:\
*.jl=:\
*.js=:\
*.json=:\
*.lua=:\
*.md=:\
*.php=:\
*.pl=:\
*.pro=:\
*.py=:\
*.rb=:\
*.rs=:\
*.scala=:\
*.ts=:\
*.vim=:\
*.cmd=:\
*.ps1=:\
*.sh=:\
*.bash=:\
*.zsh=:\
*.fish=:\
*.tar=:\
*.tgz=:\
*.arc=:\
*.arj=:\
*.taz=:\
*.lha=:\
*.lz4=:\
*.lzh=:\
*.lzma=:\
*.tlz=:\
*.txz=:\
*.tzo=:\
*.t7z=:\
*.zip=:\
*.z=:\
*.dz=:\
*.gz=:\
*.lrz=:\
*.lz=:\
*.lzo=:\
*.xz=:\
*.zst=:\
*.tzst=:\
*.bz2=:\
*.bz=:\
*.tbz=:\
*.tbz2=:\
*.tz=:\
*.deb=:\
*.rpm=:\
*.jar=:\
*.war=:\
*.ear=:\
*.sar=:\
*.rar=:\
*.alz=:\
*.ace=:\
*.zoo=:\
*.cpio=:\
*.7z=:\
*.rz=:\
*.cab=:\
*.wim=:\
*.swm=:\
*.dwm=:\
*.esd=:\
*.jpg=:\
*.jpeg=:\
*.mjpg=:\
*.mjpeg=:\
*.gif=:\
*.bmp=:\
*.pbm=:\
*.pgm=:\
*.ppm=:\
*.tga=:\
*.xbm=:\
*.xpm=:\
*.tif=:\
*.tiff=:\
*.png=:\
*.svg=:\
*.svgz=:\
*.mng=:\
*.pcx=:\
*.mov=:\
*.mpg=:\
*.mpeg=:\
*.m2v=:\
*.mkv=:\
*.webm=:\
*.ogm=:\
*.mp4=:\
*.m4v=:\
*.mp4v=:\
*.vob=:\
*.qt=:\
*.nuv=:\
*.wmv=:\
*.asf=:\
*.rm=:\
*.rmvb=:\
*.flc=:\
*.avi=:\
*.fli=:\
*.flv=:\
*.gl=:\
*.dl=:\
*.xcf=:\
*.xwd=:\
*.yuv=:\
*.cgm=:\
*.emf=:\
*.ogv=:\
*.ogx=:\
*.aac=:\
*.au=:\
*.flac=:\
*.m4a=:\
*.mid=:\
*.midi=:\
*.mka=:\
*.mp3=:\
*.mpc=:\
*.ogg=:\
*.ra=:\
*.wav=:\
*.oga=:\
*.opus=:\
*.spx=:\
*.xspf=:\
*.pdf=:\
*.nix=:\
.git/=:\
.github/=:\
.config/=:\
node_modules/=:\
"
