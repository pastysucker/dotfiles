function fish_greeting
end
starship init fish | source
set -gx BROWSER /usr/bin/vivaldi-stable
set -gx EDITOR /usr/bin/nvim

set -gx HOMEBREW_PREFIX "/home/tamim/.linuxbrew";
set -gx HOMEBREW_CELLAR "/home/tamim/.linuxbrew/Cellar";
set -gx HOMEBREW_REPOSITORY "/home/tamim/.linuxbrew/Homebrew";
set -q PATH; or set PATH ''; set -gx PATH "/home/tamim/.linuxbrew/bin" "/home/tamim/.linuxbrew/sbin" $PATH;
set -q MANPATH; or set MANPATH ''; set -gx MANPATH "/home/tamim/.linuxbrew/share/man" $MANPATH;
set -q INFOPATH; or set INFOPATH ''; set -gx INFOPATH "/home/tamim/.linuxbrew/share/info" $INFOPATH;

# eval (ssh-agent -c)

if test -n "$DESKTOP_SESSION"
    set (gnome-keyring-daemon --start | string split "=")
end

# tabtab source for packages
# uninstall by removing these lines
[ -f ~/.config/tabtab/__tabtab.fish ]; and . ~/.config/tabtab/__tabtab.fish; or true
set -gx VOLTA_HOME "$HOME/.volta"
set -gx PATH "$VOLTA_HOME/bin" $PATH
