function fish_user_key_bindings
  bind \e\[3\;5~ kill-word
  bind \cH backward-kill-word
  bind \cV beginning-of-line
  bind \f end-of-line
end

fzf_key_bindings
