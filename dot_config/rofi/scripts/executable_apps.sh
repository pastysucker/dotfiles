#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

run() {
  swaymsg exec $1
}

dir="$HOME/.config/rofi/themes/colorful_2.rasi"
rofi_command="rofi -i"

# Links
files="  File manager"
editor="  Code editor"
music="  Music player"
browser="  Web browser"
firefox="  Firefox"
vlc="嗢  VLC media player"
games="  Games"

# Error msg
msg() {
  rofi -e "$1"
}

# Variable passed to rofi
options="$editor\n$games\n$music\n$vlc\n$files\n$browser\n$firefox"

chosen="$(echo -e "$options" | $rofi_command -p " " -dmenu -selected-row 0)"
case $chosen in
    $files)
      if [[ -f /usr/bin/thunar ]]; then
        run thunar &
      elif [[ -f /usr/bin/pcmanfm ]]; then
        run pcmanfm &
      else
        msg "No suitable file manager found!"
      fi
        ;;
    $browser)
        run vivaldi-stable --force-dark-mode &
        ;;
    $games)
        run lutris &
        ;;
    $firefox)
        run firefox &
        ;;
    $editor)
      if [[ -f /usr/bin/code ]]; then
        run code --enable-features=UseOzonePlatform --ozone-platform=wayland &
      else
        msg "No suitable text editor found!"
      fi
        ;;
    $vlc)
      run vlc &
      ;;
    $music)
      if [[ -f /usr/bin/rhythmbox ]]; then
        run rhythmbox &
      else
        msg "No suitable music player found!"
      fi
        ;;
esac
