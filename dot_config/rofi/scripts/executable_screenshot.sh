#!/bin/sh

ITEMS=" Whole screen\n濾 Selected area\n类 Selected window\n練 Active window"

SELECTED=$(printf '%b' "$ITEMS" | rofi -p "🖵 " -no-show-icons -i -dmenu)

case "$SELECTED" in
  " Whole screen")
    sh -c "grimshot save screen - | swappy -f -"
    ;;
  "濾 Selected area")
    sh -c "grimshot save area - | swappy -f -"
    ;;
  "类 Selected window")
    grimshot save window - | swappy -f -
    ;;
  "練 Active window")
    grimshot save active - | swappy -f -
    ;;
esac
