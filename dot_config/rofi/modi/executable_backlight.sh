#!/usr/bin/env bash

## Author : Aditya Shakya (adi1090x)
## Mail : adi1090x@gmail.com
## Github : @adi1090x
## Reddit : @adi1090x

rofi_command="rofi -theme $HOME/.config/rofi/themes/three.rasi"

## Get Brightness
VAR="$(brillo)"
BLIGHT="$(printf "%.0f\n" "$VAR")"

if [[ $BLIGHT -ge 1 ]] && [[ $BLIGHT -le 29 ]]; then
    MSG="Low"
elif [[ $BLIGHT -ge 30 ]] && [[ $BLIGHT -le 49 ]]; then
    MSG="Optimal"
elif [[ $BLIGHT -ge 50 ]] && [[ $BLIGHT -le 69 ]]; then
    MSG="High"
elif [[ $BLIGHT -ge 70 ]] && [[ $BLIGHT -le 99 ]]; then
    MSG="Too Much"
fi

## Icons
ICON_UP=" +"
ICON_DOWN=" -"
# ICON_OPT=""

options="$ICON_UP\n$ICON_DOWN"

## Main
chosen="$(echo -e "$options" | $rofi_command -p "$BLIGHT%" -dmenu -selected-row 1)"
case $chosen in
    $ICON_UP)
        pkexec brillo -A 10 && notify-send -u low -t 1500 "Brightness Up $ICON_UP"
        ;;
    $ICON_DOWN)
        pkexec brillo -U 10 && notify-send -u low -t 1500 "Brightness Down $ICON_DOWN"
        ;;
esac

