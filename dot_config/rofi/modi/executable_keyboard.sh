#!/usr/bin/env sh

LAYOUTS="English (US),Bangla (BD)"

SELECTED=$(echo "$LAYOUTS" | rofi -m 0 -i -sep "," -dmenu -p "⌨ ")

alias signal="pkill -SIGRTMIN+2 waybar"

case $SELECTED in
  "English (US)")
    ibus engine xkb:us::eng
    signal
    ;;
  "Bangla (BD)")
    ibus engine OpenBangla
    signal
    ;;
esac
