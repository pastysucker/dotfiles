#!/bin/sh

entries=$(printf "%s\n" " Turn off screen" " Lock" " Logout" "鈴 Suspend" "勒 Reboot" "襤 Shutdown")

selected=$(echo "$entries" | tofi --prompt-text " " -c ~/.config/tofi/misc --placeholder-text "Power" | awk '{print tolower($2)}')

case $selected in
  turn)
    exec swaymsg "output * dpms off";;
  lock)
    exec swaylock -f;;
  logout)
    swaymsg exit;;
  suspend)
    exec systemctl suspend;;
  reboot)
    exec systemctl reboot;;
  shutdown)
    exec systemctl poweroff -i;;
esac
