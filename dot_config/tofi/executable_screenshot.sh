#!/bin/sh

ITEMS=$(printf "%s\n" "Whole screen" "Selected area" "Selected window" "Active window")

SELECTED=$(printf '%b' "$ITEMS" | tofi --prompt-text "度" -c ~/.config/tofi/misc --placeholder "Screenshot")

case "$SELECTED" in
  "Whole screen")
    sh -c "grimshot save screen - | swappy -f -"
    ;;
  "Selected area")
    swaymsg exec "grimshot save area - | swappy -f -"
    ;;
  "Selected window")
    grimshot save window - | swappy -f -
    ;;
  "Active window")
    grimshot save active - | swappy -f -
    ;;
esac
