#!/usr/bin/env sh

SELECTED=$(printf "%s\n" "English (US)" "Bangla (BD)" | tofi -c ~/.config/tofi/dmenu)

alias signal="pkill -SIGRTMIN+2 waybar"

case $SELECTED in
  "English (US)")
    ibus engine xkb:us::eng
    signal
    ;;
  "Bangla (BD)")
    ibus engine OpenBangla
    signal
    ;;
esac
