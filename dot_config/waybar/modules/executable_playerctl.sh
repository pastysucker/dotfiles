#!/usr/bin/env bash
exec 2> "$XDG_RUNTIME_DIR/waybar-playerctl.log"
IFS=$'\n\t'

while true; do

  while read -r playing position length artist duration; do
  echo $duration
    # json escaping
    artist="${artist//\"/\\\"}"
    ((percentage = length ? (100 * (position % length)) / length : 0))
    case $playing in
      Paused) text=" $duration" ;;
      Playing) text=" $duration" ;;
      *)text='' ;;
    esac

    # exit if print fails
    printf '{"text":"%s","tooltip":"%s","class":"%s","percentage":%s}\n' \
      "$text" "$playing: $artist" "$percentage" "$percentage" || break 2

  done < <(
    # requires playerctl>=2.0
    playerctl --follow metadata --format \
      $'{{status}}\t{{position}}\t{{mpris:length}}\t{{markup_escape(artist)}} - {{markup_escape(title)}}\t{{duration(position)}} - {{duration(mpris:length)}}' &
    echo $! > "$XDG_RUNTIME_DIR/waybar-playerctl.pid"
  )

  # no current players
  # exit if print fails
  echo '<span foreground=#dc322f>⏹</span>' || break
  sleep 15

done

read -r < "$XDG_RUNTIME_DIR/waybar-playerctl.pid"
kill "$REPLY"
