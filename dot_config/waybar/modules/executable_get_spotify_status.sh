#!/bin/bash

if [ "$(playerctl --player=playerctld status)" = "Stopped" ]; then
    echo "{\"text\": \"\"}"
elif [ "$(playerctl --player=playerctld status)" = "Paused"  ]; then
    # polybar-msg -p "$(pgrep -f "polybar now-playing")" hook spotify-play-pause 2 >/dev/null
    DURATION=$(playerctl --player=playerctld metadata --format "{{ duration(position) }} - {{ duration(mpris:length) }}")
    DETAILS=$(playerctl --player=playerctld metadata --format "{{ artist }} - {{ title }}")
    echo "{\"text\": \"$DURATION\", \"tooltip\": \"$DETAILS\"}"
    # playerctl --player=playerctld metadata --format "{{ title }} - {{ artist }}"
else # Can be configured to output differently when player is paused
    DURATION=$(playerctl --player=playerctld metadata --format "{{ duration(position) }} - {{ duration(mpris:length) }}")
    DETAILS=$(playerctl --player=playerctld metadata --format "{{ artist }} - {{ title }}")
    echo "{\"text\": \"$DURATION\", \"tooltip\": \"$DETAILS\"}"
fi
