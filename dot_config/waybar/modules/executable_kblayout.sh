#!/usr/bin/env bash

ENGINE=$(ibus engine)

case $ENGINE in
  xkb:us::eng)
    LAYOUT="US"
    LANG="EN"
    ;;

  OpenBangla)
    LAYOUT="BD"
    LANG="BN"
    ;;

  *)
    LAYOUT="Unknown"
    LANG="Unknown"
    ENGINE="Not Started"
    ;;
esac

# Other ways of serializing JSON
# JSON="{ \"text\": \"$LANG\", \"tooltip\": \"Layout: $LAYOUT\nEngine: $ENGINE\", \"class\": \"lang_${LANG}_$LAYOUT\" }"
# JSON=$(jq -n \
#         --arg layout $LAYOUT \
#         --arg engine $ENGINE \
#         --arg lang $LANG \
#         '{ text: $lang, tooltip: "Layout: \($layout)\nEngine: \($engine)", class: "lang_\($lang)_\($layout)" }')
JSON=$(jo \
        text=$LANG \
        tooltip="$(printf "Layout: %s-%s\nEngine: %s" "$LANG" "$LAYOUT" "$ENGINE")" \
        class="lang_${LANG}_$LAYOUT")

if [[ $ENGINE == "Not Started" ]]; then
  echo ""
else
  echo "$JSON"
fi
