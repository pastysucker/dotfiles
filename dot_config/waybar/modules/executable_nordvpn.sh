#!/bin/bash

STATUS=$(nordvpn status)

CONNECTION_STATUS=$(echo $STATUS | grep -o '\w*onnected\b')

COUNTRY=$(echo $STATUS | pcre2grep -o1 'Country:\s(.*)\b')
CITY=$(echo $STATUS | pcre2grep -o1 'City:\s(.*)')
IP=$(echo $STATUS | pcre2grep -o1 'IP:\s(.*)')

echo "{\"text\": \"$(echo $CONNECTION_STATUS)\", \"tooltip\": \"Country: $(echo $CITY)\"}"

