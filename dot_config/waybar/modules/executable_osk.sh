#!/usr/bin/env bash

if pgrep -x "wvkbd-mobintl" > /dev/null; then
  killall wvkbd-mobintl
else
  "$HOME/.local/bin/wvkbd-mobintl"
fi
