#!/usr/bin/env sh

# Terminate running instances
killall -q waybar

# Wait until processes have been killed
while pgrep -x waybar >/dev/null; do sleep 1; done

waybar
